from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.utils.datastructures import MultiValueDict

from hashlib import sha1
from moto import mock_s3
from soundtests.forms import SoundForm
from soundtests.models import Sound

import boto
import boto3
import os

class SoundFormValidate(TestCase):
    def setUp(self):
        self.path = os.path.abspath('tests/media/{}')
        self.sound_info = {
                "phrase1": "Test Sound",
                "phrase2": "Test Sound",
                "language": "ENGLISH",
                "num_words": 2,
                "num_syllables": 2,
                "sex": "M",
                "voice": False,
                "speaker_name": "Test Speaker",
        }

        self.test_sound = open(self.path.format("test_sound.wav"), "rb").read()
        self.test_sound_too_large = open(self.path.format("test_sound_too_large.wav"), "rb").read()

    def test_form_validates_with_good_info(self):
        file_data = MultiValueDict({"sounds_to_upload": [
            SimpleUploadedFile("test_sound_1.wav", self.test_sound, content_type="audio/x-wav"),
            SimpleUploadedFile("test_sound_2.wav", self.test_sound, content_type="audio/wav")
        ]})
        form = SoundForm(data=self.sound_info, files=file_data)
        self.assertTrue(form.is_valid())

    def test_form_rejects_file_too_large(self):
        file_data = MultiValueDict({"sounds_to_upload": [
            SimpleUploadedFile("test_sound.wav", self.test_sound, content_type="audio/x-wav"),
            SimpleUploadedFile("test_sound_too_large.wav", self.test_sound_too_large, content_type="audio/x-wav")
        ]})
        form = SoundForm(data=self.sound_info, files=file_data)
        self.assertIn("File test_sound_too_large.wav is too large", form.errors["__all__"][0])

    def test_form_rejects_non_wav_content(self):
        file_data = MultiValueDict({"sounds_to_upload": [
            SimpleUploadedFile("test_sound_1.wav", self.test_sound, content_type="audio/x-wav"),
            SimpleUploadedFile("test_sound_2.wav", self.test_sound, content_type="audio/wav"),
            SimpleUploadedFile("test_sound_3.wav", self.test_sound, content_type="non-audio")
        ]})
        form = SoundForm(data=self.sound_info, files=file_data)
        self.assertIn("File test_sound_3.wav is not a WAV file", form.errors["__all__"][0])

    def test_form_returns_correct_hash(self):
        test_hashes = {"a": sha1(b"a").hexdigest(), "b": sha1(b"b").hexdigest(), "c": sha1(b"c").hexdigest()}

        file_data = MultiValueDict({"sounds_to_upload": [
            SimpleUploadedFile("test_sound1.wav", b"a", content_type="audio/x-wav"),
            SimpleUploadedFile("test_sound2.wav", b"b", content_type="audio/x-wav"),
            SimpleUploadedFile("test_sound3.wav", b"c", content_type="audio/x-wav")
        ]})
        form = SoundForm(data=self.sound_info, files=file_data)
        self.assertTrue(form.is_valid()) 
        for key, value in test_hashes.items():
            self.assertEquals(key, form.cleaned_data["hashed_sounds"][value].decode())

class SoundFormSave(TestCase):
    mock_s3 = mock_s3()
    def setUp(self):
        self.sound_info = {
                "phrase1": "Uploaded Sound",
                "phrase2": "Uploaded Sound",
                "language": "ENGLISH",
                "num_words": 2,
                "num_syllables": 2,
                "sex": "M",
                "voice": False,
                "speaker_name": "Test Speaker",
        }

        self.mock_s3.start()
        self.s3 = boto3.Session(aws_access_key_id="fakeaccess", aws_secret_access_key="fakesecret")
        self.s3.resource("s3").create_bucket(Bucket="test_bucket")
        self.bucket = self.s3.resource("s3").Bucket(name="test_bucket")
        
    def tearDown(self):
        self.mock_s3.stop()

    def test_form_correctly_saves_to_db_and_uploads_sound_to_s3(self):
        test_hash = sha1(b"abc123").hexdigest()
        file_data = MultiValueDict({"sounds_to_upload": [
            SimpleUploadedFile("test_sound.wav", b"abc123", content_type="audio/x-wav")
        ]})
        form = SoundForm(data=self.sound_info, files=file_data)
        form.is_valid()
        saved_sound = form.save(bucket=self.bucket)
        sound = Sound.objects.get(sound_hash=test_hash)
        response = self.s3.resource("s3").Object(self.bucket.name, sound.key).get()

        self.assertEquals(test_hash, saved_sound[0].sound_hash)
        self.assertEquals(test_hash, sha1(response["Body"].read()).hexdigest())

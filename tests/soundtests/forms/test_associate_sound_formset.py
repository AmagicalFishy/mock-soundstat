from django.forms import formset_factory
from django.test import TestCase
from model_mommy import mommy

from soundtests.forms import AssociateSoundForm, AssociateSoundFormset
from soundtests.models import SoundTest

class TestAssociateSoundFormset(TestCase):
    def setUp(self):
        self.data = { 
                'form-TOTAL_FORMS': '2',
                'form-INITIAL_FORMS': '2',
                'form-MAX_NUM_FORMS': '2',
                'form-0-sound_language': 'Body Language',
                'form-0-sound_pk': 'm1',
                'form-0-master_phrase1': 'Choomba',
                'form-0-master_phrase2': 'Woomba',
                'form-0-sounds_to_send': ['s1', 's2', 's3'],
                'form-1-sound_language': 'Elvish',
                'form-1-sound_pk': 'm2',
                'form-1-master_phrase1': 'Hello. Would you like to buy a bag of peanuts?',
                'form-1-master_phrase2': 'If you plant them, happiness will grow!',
                'form-1-sounds_to_send': ['s4', 's5'],
        }
    
        # Create mock lessons/sounds
        lesson_1 = mommy.make('Lesson')
        lesson_2 = mommy.make('Lesson')
        mommy.make(
                'Sound', 
                pk='m1', 
                phrase1='Choomba',
                phrase2='Woomba',
                language='Body Language',
                associated_lesson=lesson_1,
        )
        mommy.make(
                'Sound', 
                pk='m2',
                phrase1='Hello. Would you like to buy a bag of peanuts?',
                phrase2='If you plant them, happiness will grow!',
                language='Elvish',
                associated_lesson=lesson_2
        )
        for pk in ['s1', 's2', 's3', 's4', 's5']:
            mommy.make('Sound', pk=pk)

        # Create formset
        self.AssociateSoundFormset_factory = formset_factory(
                form=AssociateSoundForm, 
                formset=AssociateSoundFormset,
                extra=0
        )
        self.formset = self.AssociateSoundFormset_factory(
                data=self.data,
                test_data={'name': 'Test Test', 'description': 'Big and red with stripes'}
        )
    
    def test_associate_sound_formset_validates_with_good_data(self):
        self.assertTrue(self.formset.is_valid())

    def test_lesson_sound_formset_does_not_validate_when_sound_to_send_is_not_selected(self):
        bad_data = self.data.copy()
        bad_data['form-1-sounds_to_send'] = []
        formset = self.AssociateSoundFormset_factory(
                data=bad_data,
                test_data={'name': 'Test Test', 'description': 'Fat'}
        )
        formset.is_valid()
        self.assertIn('This field is required', formset.errors[1]['sounds_to_send'][0])

    def test_associate_sound_formset_appropriately_saves_test(self):
        self.assertTrue(self.formset.is_valid())
        self.formset.save()
        test = SoundTest.objects.get(name='Test Test')
        self.assertEquals(self.formset.test_name, test.name)
        self.assertEquals(self.formset.test_description, test.description)
        self.assertEquals(5, len(test.pairs_to_test.all()))



import os

from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from soundtests.forms import LessonForm
from soundtests.models import Lesson

class LessonFormValidate(TestCase):
    def setUp(self):
        self.path = os.path.abspath('tests/media/{}')
        self.lesson_info = {"lesson_name": "Test Lesson Name"}
        self.lesson = Lesson(lesson_name="Willy Wonka's Treats")
        self.lesson.save()

    def test_form_validates_with_valid_json(self):
        test_json = open(self.path.format("test_lesson.json"), "rb").read()
        file_data = {"lesson_json_file": SimpleUploadedFile("test_json.json", test_json)}
        form = LessonForm(data=self.lesson_info, files=file_data)
        self.assertTrue(form.is_valid())

    def test_form_validation_fails_with_invalid_json(self):
        test_json_invalid = open(self.path.format("test_json_invalid.json"), "rb").read()
        file_data = {"lesson_json_file": SimpleUploadedFile("test_json_invalid.json", test_json_invalid)}
        form = LessonForm(data=self.lesson_info, files=file_data)
        self.assertFalse(form.is_valid())

    def test_form_validation_fails_with_missing_field(self):
        test_json_missing_field = open(self.path.format("test_json_missing_field.json"), "rb").read()
        file_data = {"lesson_json_file": SimpleUploadedFile("test_json_missing_field.json", test_json_missing_field)}
        form = LessonForm(data=self.lesson_info, files=file_data)
        self.assertFalse(form.is_valid())

    def test_form_validation_fails_with_already_existing_lesson_name(self):
        test_json = open(self.path.format("test_lesson.json"), "rb").read()
        file_data = {"lesson_json_file": SimpleUploadedFile("test_json.json", test_json)}
        form = LessonForm(data={"lesson_name": "Willy Wonka's Treats"}, files=file_data)
        self.assertFalse(form.is_valid())

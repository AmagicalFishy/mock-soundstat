from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import SimpleUploadedFile
from django.forms import formset_factory
from django.test import TestCase
from hashlib import sha1
from moto import mock_s3
from soundtests.forms import LessonSoundForm, LessonSoundFormset
from soundtests.models import Lesson
from soundtests.models import Sound

import os
import boto
import boto3

class TestLessonSoundForm(TestCase):
    def setUp(self):
        self.sound_data = {
                "phrase1": "Poop",
                "phrase2": "Knuckle",
                "num_words": 1,
                "num_syllables": 7,
                "lesson_url": "www.wizards.com",
                "lesson_hash": "8675309"
        }

    def test_lesson_sound_form_validates_with_good_data(self):
        form = LessonSoundForm(data=self.sound_data)
        self.assertTrue(form.is_valid())

    def test_lesson_sound_form_phrase_fields_are_immutable(self):
        form = LessonSoundForm(data=self.sound_data)
        form.initial["phrase1"] = "Change"
        form.initial["phrase2"] = "Malicious or not"
        self.assertTrue(form.is_valid())
        self.assertEquals(self.sound_data["lesson_url"], form.cleaned_data["lesson_url"])
        self.assertEquals(self.sound_data["lesson_hash"], form.cleaned_data["lesson_hash"])

    def test_lesson_sound_form_save_method_returns_appropriate_field_values(self):
         form = LessonSoundForm(data=self.sound_data)
         self.assertTrue(form.is_valid())
         self.assertEquals("Poop", form.cleaned_data["phrase1"])
         self.assertEquals("Knuckle", form.cleaned_data["phrase2"])
         self.assertEquals(1, form.cleaned_data["num_words"])
         self.assertEquals(7, form.cleaned_data["num_syllables"])
         self.assertEquals(self.sound_data["lesson_url"], form.cleaned_data["lesson_url"])
         self.assertEquals(self.sound_data["lesson_hash"], form.cleaned_data["lesson_hash"])

class TestLessonSoundFormset(TestCase):
    mock_s3 = mock_s3()
    def setUp(self):
        self.lesson_dict = {
                "lesson_name": "Test Lesson",
                "json_name": "Test Lesson JSON Name",
                "activity_type": "pron",
                "learning_language": "Cats Meowing",
                "known_language": "body language"
        }
        self.data = { 
                "speaker_name": "John Smith",
                "sex": "M",
                "voice": True,
                "form-TOTAL_FORMS": "2",
                "form-INITIAL_FORMS": "2",
                "form-MAX_NUM_FORMS": "2",
                "form-0-phrase1": "Poop",
                "form-0-phrase2": "Knuckle",
                "form-0-num_words": 1,
                "form-0-num_syllables": 7,
                "form-0-lesson_url": "[REDACTED]",
                "form-0-lesson_hash": "8675309",
                "form-1-phrase1": "Boy",
                "form-1-phrase2": "George",
                "form-1-num_words": 3,
                "form-1-num_syllables": 9,
                "form-1-lesson_url": "[REDACTED]",
                "form-1-lesson_hash": "ICUP"
        }

        
        # Set up fake S3 stuff
        self.mock_s3.start()
        self.s3 = boto3.Session(aws_access_key_id="fakeaccess", aws_secret_access_key="fakesecret")
        self.s3.resource("s3").create_bucket(Bucket="test_bucket")
        self.bucket = self.s3.resource("s3").Bucket(name="test_bucket")
        
        # Create formset
        self.LessonSoundFormset_factory = formset_factory(
                form=LessonSoundForm, 
                formset=LessonSoundFormset
        )
        self.formset = self.LessonSoundFormset_factory(
                data=self.data,
                lesson_dict=self.lesson_dict
        )


    def test_lesson_sound_formset_subform_and_lesson_dict_are_created(self): 
        self.assertDictEqual(self.formset.lesson_dict, self.lesson_dict)
        self.assertTrue(self.formset.subform)

    def test_lesson_sound_formset_subform_validates_with_good_data(self):
        self.assertTrue(self.formset.subform.is_valid())
  
    def test_lesson_sound_formset_subform_validates_with_formset_is_valid_method(self):
        subform_cleaned_data = {
                "speaker_name": "John Smith",
                "sex": "M",
                "voice": True
        }
        self.assertTrue(self.formset.is_valid())
        self.assertDictEqual(subform_cleaned_data, self.formset.subform.cleaned_data)

    def test_lesson_sound_formset_subform_cleaned_data_is_passed_to_formset_cleaned_data(self):
        subform_cleaned_data = {
                "speaker_name": "John Smith",
                "sex": "M",
                "voice": True
        }
        self.formset.is_valid()
        self.assertDictEqual(self.formset.cleaned_data[-1], subform_cleaned_data)

    def test_lesson_sound_formset_validates_with_good_data(self):
        self.assertTrue(self.formset.is_valid())

    def test_lesson_sound_formset_does_not_validate_with_improper_subform_data(self):
        bad_data = self.data
        bad_data["sex"] = "m"
        formset = self.LessonSoundFormset_factory(
                data=self.data,
                lesson_dict=self.lesson_dict
        )
        self.assertFalse(formset.is_valid())

    def test_lesson_sound_formset_subform_passes_errors_onto_formset_errors(self):
        bad_data = self.data
        bad_data["sex"] = "m"
        formset = self.LessonSoundFormset_factory(
                data=self.data,
                lesson_dict=self.lesson_dict
        )
        self.assertIn("Select a valid choice", formset.non_form_errors()[0])

    def test_lesson_sound_formset_correctly_uploads_to_s3_and_saves_all_objects(self):
        self.formset.is_valid()
        saved = self.formset.save(bucket=self.bucket)

        # Lesson
        self.assertEquals(saved["lesson"].lesson_name, "Test Lesson")
        self.assertEquals(saved["lesson"].json_name, "Test Lesson JSON Name")
        self.assertEquals(saved["lesson"].activity_type, "pron")
        self.assertEquals(saved["lesson"].learning_language, "Cats Meowing")
        self.assertEquals(saved["lesson"].known_language, "body language")

        self.assertTrue(Lesson.objects.filter(lesson_name="Test Lesson").exists())

        # Sounds
        self.assertEquals(saved["sounds"][0].sound_hash, "2e420225489cf1270189585200f2c8e516d13554")
        self.assertEquals(saved["sounds"][0].speaker_name, "John Smith")
        self.assertEquals(saved["sounds"][1].sound_hash, "dfa81007466471eb0604f59eae82d369bac69d74")
        self.assertEquals(saved["sounds"][1].speaker_name, "John Smith")

from django.test import TestCase
from model_mommy import mommy

from soundtests.forms import SoundTestForm
from soundtests.models import Sound, Lesson

class TestSoundTestForm(TestCase):
    def setUp(self):
        self.lesson = mommy.make('Lesson')
        mommy.make('Sound', associated_lesson=self.lesson, pk='s1')
        mommy.make('Sound', associated_lesson=self.lesson, pk='s2')
        mommy.make('Sound', associated_lesson=self.lesson, pk='s3')
        mommy.make('Sound', pk='s4') # No lesson

        self.data = {
                'name': "Name of a Test",
                'description': "Description of a test. It is very detailed and descriptive.",
                'sounds_to_send': ['s1', 's2', 's3'],
        }

    def test_form_validates_with_good_info(self):
        form = SoundTestForm(data=self.data)
        self.assertTrue(form.is_valid())

    def test_form_saves_if_master_to_master_is_checked(self):
        form = SoundTestForm(data={**self.data, **{'master-to-master': True}})
        self.assertTrue(form.is_valid())
        test = form.save()
        self.assertEquals(test.name, self.data.get('name'))
        self.assertEquals(test.description, self.data.get('description'))
        self.assertEquals(test.associated_lessons.all()[0], self.lesson)

    def test_form_does_not_validate_if_test_name_already_exists(self):
        form = SoundTestForm(data={**self.data, **{'master-to-master': True}})
        form.is_valid()
        form.save()
        re_form = SoundTestForm(data=self.data)
        re_form.is_valid()
        self.assertIn("There already exists a test with that name", re_form.errors['__all__'][0])

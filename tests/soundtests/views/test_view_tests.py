from django.test import TestCase
from django.urls import reverse

class TestViewTests(TestCase):
    def test_view_tests_page_loads(self):
        response = self.client.get(reverse('soundtests:view_tests'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "soundtests/view_tests.html")

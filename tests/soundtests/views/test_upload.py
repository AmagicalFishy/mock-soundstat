from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, TestCase
from django.urls import reverse

from soundtests.views import upload_sound

import os

class HomePageTest(TestCase):
    def test_home_page_loads(self):
        response = self.client.get(reverse("soundtests:index"), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "soundtests/index.html")

class UploadSoundTest(TestCase):
    def test_upload_sound_loads_on_get(self):
        response = self.client.get(reverse("soundtests:upload_sound"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "soundtests/upload_sound.html")

class UploadLessonTest(TestCase):
    def test_upload_lesson_loads_on_get(self):
        response = self.client.get(reverse("soundtests:upload_lesson"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "soundtests/upload_lesson.html")

    def test_upload_lesson_redirects_to_upload_lesson_sounds_on_valid_form_submit(self):
        test_lesson_json = open(os.path.abspath("tests/media/test_upload_lesson_sounds.json"), "rb").read()
        form_data = {
                "lesson_name": "Test Lesson",
                "lesson_json_file": SimpleUploadedFile("test_lesson.json", test_lesson_json)
        }
        response = self.client.post(reverse("soundtests:upload_lesson"), form_data)
        self.assertRedirects(response, reverse('soundtests:upload_lesson_sounds'))

class UploadLessonSoundsTest(TestCase):
    def test_upload_lesson_sounds_redirects_to_upload_lesson_when_no_cleaned_data_is_in_session(self):
        response = self.client.get(reverse("soundtests:upload_lesson_sounds"))
        self.assertRedirects(response, reverse('soundtests:upload_lesson'))

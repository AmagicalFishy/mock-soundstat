from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

class TestViewTestContents(TestCase):
    def setUp(self):
        self.sound_test = mommy.make('SoundTest')

    def test_view_test_contents_page_loads(self):
        response = self.client.get(reverse('soundtests:view_test_contents', kwargs={'test_slug': self.sound_test.slug}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "soundtests/view_test_contents.html")

from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

from soundtests.views import create_test
from soundtests.models import Sound

class TestCreateTestView(TestCase):
    def setUp(self):
        self.lesson = mommy.make('Lesson')
        mommy.make('Sound', associated_lesson=self.lesson, pk='s1')
        mommy.make('Sound', associated_lesson=self.lesson, pk='s2')
        mommy.make('Sound', associated_lesson=self.lesson, pk='s3')
        mommy.make('Sound', pk='s4') # No lesson

        self.data = {
                'form-0-name': "Name of a Test",
                'form-0-description': "Description of a test. It is very detailed and descriptive.",
                'form-0-sounds_to_send': ['s1', 's2', 's3'],
        }

    def test_view_redirects_to_associate_sounds_on_valid_form_submit(self):
        response = self.client.post(reverse('soundtests:create_test'), self.data, follow=True)
        self.assertRedirects(response, expected_url=reverse('soundtests:associate_sounds'))

    def test_view__redirects_to_index_on_valid_master_to_master_form_submit(self):
        response = self.client.post(reverse('soundtests:create_test'), {**self.data, **{'form-0-master_to_master_test': True}})
        self.assertRedirects(response, expected_url=reverse('soundtests:index'))

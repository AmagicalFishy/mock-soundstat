from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, TestCase
from django.urls import reverse

from model_mommy import mommy
from model_mommy.recipe import Recipe, foreign_key


from soundtests.models.sound import Sound
from soundtests.models.lesson import Lesson

import json
import os

class SoundFilterTest(TestCase):
    def setUp(self):
        self.lesson = mommy.make("Lesson")

        # Single filter
        mommy.make("Sound", phrase1="SIN TEST", language="BoloMcFrolo", _quantity=5)

        # Double filter
        mommy.make(
                "Sound", 
                phrase1="MUL TEST",
                speaker_name="Steiglewitz Finstermeister", 
                num_words=7, 
                _quantity=5,
        )
        
        # Associated w/ Lesson
        mommy.make(
                "Sound", 
                phrase1="FFFFFFUUUUUUUU", 
                associated_lesson=self.lesson, 
                language="love", 
                _quantity=10
        )
        mommy.make("Sound", phrase1="chips", language="love", _quantity=10)

    def test_sound_single_filter_returns_appropriate_sounds(self):
        response = self.client.get(
                reverse("soundtests:sound_filter"),
                {
                    "recording": "false",
                    "language[]": "BoloMcFrolo"
                },
                HTTP_X_REQUESTED_WITH="XMLHttpRequest"
        )
        self.assertEqual(response.status_code, 200)
        response_json = json.loads(response.content.decode("utf-8"))
        self.assertEquals(1, len(response_json))
        sound_phrase = response_json[0]["phrase1"]

        response = self.client.get(
                reverse("soundtests:sound_filter"),
                {
                    "recording": "true",
                    "unique_phrase[]": sound_phrase,
                    "language[]": "BoloMcFrolo"
                },
                HTTP_X_REQUESTED_WITH="XMLHttpRequest"
        )
        response_json = json.loads(response.content.decode("utf-8"))
        for sound_data in response_json:
            self.assertEqual("BoloMcFrolo", sound_data["language"])

        self.assertEqual(5, len(response_json))

    def test_sound_double_filter_returns_appropriate_sounds(self):
        response = self.client.get(
                reverse("soundtests:sound_filter"),
                {
                    "recording": "false",
                    "speaker[]": "Steiglewitz Finstermeister",
                    "num_words": 7,
                },
                HTTP_X_REQUESTED_WITH="XMLHttpRequest"
        )
        self.assertEqual(response.status_code, 200)
        response_json = json.loads(response.content.decode("utf-8"))
        self.assertEquals(1, len(response_json))
        sound_phrase = response_json[0]["phrase1"]

        response = self.client.get(
                reverse("soundtests:sound_filter"),
                {
                    "recording": "true",
                    "unique_phrase[]": sound_phrase,
                    "speaker[]": "Steiglewitz Finstermeister",
                    "num_words": 7
                },
                HTTP_X_REQUESTED_WITH="XMLHttpRequest"
        )
        response_json = json.loads(response.content.decode("utf-8"))
        for sound_data in response_json:
            self.assertEqual("MUL TEST", sound_data["phrase1"])

        self.assertEqual(5, len(response_json))

    def test_sounds_in_lessons_are_correctly_filtered(self):
        response = self.client.get(
                reverse("soundtests:sound_filter"),
                {
                    "recording": "false",
                    "lessons": "true",
                    "language[]": "love",
                },
                HTTP_X_REQUESTED_WITH="XMLHttpRequest"
        )
        self.assertEqual(response.status_code, 200)
        response_json = json.loads(response.content.decode("utf-8"))
        self.assertEquals(1, len(response_json))
        sound_phrase = response_json[0]["phrase1"]
        response = self.client.get(
                reverse("soundtests:sound_filter"),
                {
                    "recording": "true",
                    "unique_phrase[]": sound_phrase,
                    "language[]": "love",
                },
                HTTP_X_REQUESTED_WITH="XMLHttpRequest"
        )
        response_json = json.loads(response.content.decode("utf-8"))
        for sound_data in response_json:
            self.assertEqual("love", sound_data["language"])

        self.assertEqual(10, len(response_json))

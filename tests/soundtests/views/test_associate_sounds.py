from django.urls import reverse
from django.test import TestCase
from model_mommy import mommy

import os

class TestAssociateSounds(TestCase):
    def setUp(self):
        self.test = mommy.make('SoundTest')
        self.sounds = mommy.make('Sound', _quantity=10)

    def test_associate_sounds_page_loads(self):
        # Set up session variable (which mimics the redirect from the 
        # SoundTest creation page
        session = self.client.session
        session['test_name'] = self.test.name
        session['test_description'] = self.test.description
        session['test_keys'] = [sound.pk for sound in self.sounds]
        session.save()

        # Actual test
        response = self.client.get(reverse('soundtests:associate_sounds'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "soundtests/associate_sounds.html")

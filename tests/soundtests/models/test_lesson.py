import random

from soundtests.models.sound import Sound
from soundtests.models.lesson import Lesson

from django.test import TestCase

from utils import random_gen

class TestLesson(TestCase):
    def setUp(self):
        self.maxDiff = None
        self.lesson = Lesson(
                lesson_name="Tom",
                json_name = "Thomas",
                activity_type="pron",
                learning_language="Celtic",
                known_language="Engrosh"
        )
        self.lesson.save()
        
    def test_json(self):
        correct_json = {
                "listName": "Thomas",
                "languageLearn": "Celtic",
                "languageNative": "Engrosh",
                "files": [],
                "os": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
                "userName": "",
                "activityType": "pron"
        }
        self.assertDictEqual(correct_json, self.lesson.json)


from model_mommy import mommy
from django.test import TestCase

import random

class TestSound(TestCase):
    def setUp(self):
        self.test = mommy.make('SoundTest')
        self.msound1 = mommy.make('Sound', pk='M1')
        self.msound2 = mommy.make('Sound', pk='M2')
    
        self.num = random.randint(2, 15)

        for ii in range(0, self.num):
            mommy.make(
                    'TestPair',
                    associated_test=self.test,
                    master_sound=self.msound1,
                    user_sound=mommy.make('Sound')
            )
            mommy.make(
                    'TestPair',
                    associated_test=self.test,
                    master_sound=self.msound2,
                    user_sound=mommy.make('Sound')
            )

    def test_master_sounds_property(self):
        self.assertTrue(2, len(self.test.master_sounds))

from utils import random_gen
from django.test import TestCase
from moto import mock_s3
from soundtests.models.sound import Sound
from soundtests.models.lesson import Lesson

import botocore
import boto3
import random

class TestSound(TestCase):
    @classmethod
    def setUpClass(cls):
        """Sets up fake S3 Bucket"""
        cls.mock_s3 = mock_s3()
        cls.mock_s3.start()
        cls.s3 = boto3.Session(aws_access_key_id='fakeaccess', aws_secret_access_key='fakesecret')
        cls.s3.resource('s3').create_bucket(Bucket='test_bucket')
        cls.bucket = cls.s3.resource('s3').Bucket(name='test_bucket')

    @classmethod
    def tearDownClass(cls):
        cls.mock_s3.stop()

    def setUp(self):
        self.sound = Sound(
                phrase1 = "Test",
                phrase2 = "Test",
                language = "CHILLDOP KITTERSTANK",
                num_words = random.randrange(10),
                num_syllables = random.randrange(10),
                sex = random.choice(["M", "F"]),
                voice = random.choice([True, False]),
                speaker_name = random_gen(),
                sound_hash = "lol"
        )
        self.lesson = Lesson(
                lesson_name="Fopping Groff",
                json_name = "Shelgog Greth Fack-Nowell!!",
                activity_type="pron",
                learning_language="VISUAL",
                known_language="CPP"
        )
        self.lesson.save()

        self.sound.save()

    def test_key_property(self):
        self.assertEquals("[REDACTED]", self.sound.key)

    def test_generate_url_with_no_lesson_association(self):
        self.assertEquals(
                "[REDACTED]",
                self.sound.url
        )

    def test_generate_url_with_lesson_association(self):
        self.sound.associated_lesson = self.lesson
        self.sound.lesson_url = "www.wutangclanaintnothingtofuckwith.com"
        self.sound.save()
        self.assertEquals("www.wutangclanaintnothingtofuckwith.com", self.sound.url)

    def test_json_with_no_lesson_association(self):
        self.assertEquals("This sound is not associated with a lesson", self.sound.json["ERR"])

    def test_json_with_lesson_association(self):
        self.sound.associated_lesson = self.lesson
        self.sound.lesson_url = "www.pen-island.com"
        self.sound.lesson_hash = "88888888"
        self.sound.save()

        correct_json = {
                "url": "www.pen-island.com",
                "priority": 0,
                "hash": "88888888",
                "phrase1": "Test",
                "phrase2": "Test",
                "phraseTranslit": "",
                "learningLanguageCode": "VISUAL",
                "knownLanguageCode": "CPP",
                "precompareScore": 0
        }

        self.assertDictEqual(correct_json, self.sound.json)

    def test_error_is_thrown_if_language_or_phrase2_field_changed_without_bucket_argument(self):
        self.sound.phrase2 = "casette tapes in a VCR"
        self.assertRaises(TypeError, self.sound.save)

    def test_s3_key_is_properly_updated_when_fields_are_changed(self):
        self.bucket.put_object(Key=self.sound.key, Body='sound data', ContentType='text/plain')
        self.sound.language = "la mama la googina"
        self.sound.phrase2 = "lou la batsu la ba ledina"
        self.sound.save(bucket=self.bucket)
        self.assertEquals('[REDACTED]', self.sound.key)

        s3 = self.s3.resource('s3')
        # Old key does not exist
        self.assertRaises(
                botocore.exceptions.ClientError,
                s3.Object('test_bucket', '[REDACTED]').load
        )
        # New key does exist
        sound = s3.Object('test_bucket', self.sound.key).get()['Body'].read()
        self.assertEquals(b'sound data', sound)

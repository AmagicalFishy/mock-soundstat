from model_mommy import mommy
from model_mommy.recipe import foreign_key, Recipe, related 
from soundtests.models import Sound, SoundTest, TestPair

"""
This file defines recipes for model_mommy to use. We do this because multiple
tests use the same models which sometimes need more complicated setup. Defining
a recipe saves us from having to re-write that setup every time we write a test
that uses the respective model
"""

### Recipe for fully populated SoundTest ###
# Create sounds
sound = Recipe(Sound)

# Create 5 test pairs
test_pairs = []
for ii in range(0, 5):
    test_pairs.append(Recipe(
        TestPair, 
        master_sound = foreign_key(sound),
        user_sound = foreign_key(sound)
    ))

# Create a SoundTest with these five test pairs
sound_test = Recipe(SoundTest, pairs_to_test = related(*test_pairs))
###

from soundtests.models import TestConfig, TestPair
from soundcomm.tasks.server_comm import ServerComm
from utils.builders import build_sound

class config:
    def __init__(self):
        self.added_silence = ''

c = config()
s = ServerComm(server=TestConfig.SERVER_CHOICES[0][0])
pair = TestPair.objects.get(pk=666)
s.set_lesson(pair.user_sound.associated_lesson)

user_sound = build_sound(pair.user_sound.url, c)
byte_response = s.compare_sounds(user_sound=user_sound, master_sound=pair.master_sound, method="pro").text
print(byte_response)

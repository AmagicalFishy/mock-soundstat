from selenium import webdriver
from django.conf import settings
from django.test import LiveServerTestCase
from model_mommy import mommy

import os
import random

class UploadSoundTest(LiveServerTestCase):
    def setUp(self):
        # Set up browser
        geckodriver_path = os.path.join(settings.BASE_DIR, "tests", "geckodriver")
        self.browser = webdriver.Firefox(executable_path=geckodriver_path)

        # Populate Database
        sounds = mommy.make('Sound', _quantity=100) 
        detail_test = mommy.make('SoundTest', name='AAAAAAAAAA - Test')
        pairs = []
        for ii in range(100):
            detail_test.pairs_to_test.add(
                    mommy.make(
                        'TestPair', 
                        master_sound=random.choice(sounds), 
                        user_sound=random.choice(sounds)
                    )
            )

        for pair in pairs:
            detail_test.pairs_to_test.add(pair)
        detail_test.save()

        mommy.make('SoundTest', _quantity=50)

    def tearDown(self):
        self.browser.close()

    def test_view_list_of_tests_and_view_contents_of_particular_test(self):
        # User navigates to homepage
        self.browser.get("{}/soundtests".format(self.live_server_url))

        # User clicks "View Test Contents" link and is brought to appropriate web-page
        self.browser.find_element_by_xpath('//a[contains(text(), "View Test Contents")]').click()

        # User clicks next button and is redirected
        self.browser.find_element_by_xpath('//a[contains(text(), "next")]').click()

        # User clicks previous buttona nd is redirected
        self.browser.find_element_by_xpath('//a[contains(text(), "previous")]').click()

        # User clicks on a test and is lead to its specific contents
        self.browser.find_element_by_xpath('//a[contains(text(), "AAAAAAAAAA - Test")]').click()

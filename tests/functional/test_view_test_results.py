from selenium import webdriver
from django.conf import settings
from django.test import LiveServerTestCase
from django.utils import timezone
from model_mommy import mommy

import os
import random

class ViewTestResultsTest(LiveServerTestCase):
    def setUp(self):
        # Set up browser
        geckodriver_path = os.path.join(settings.BASE_DIR, "tests", "geckodriver")
        self.browser = webdriver.Firefox(executable_path=geckodriver_path)

        # Populate Database
        self.test = mommy.make('SoundTest', pk=1, name='Completed Test')
        self.config = mommy.make(
                'TestConfig', 
                associated_test=self.test, 
                jsession_id='Desired Run',
                date=timezone.now()
        )
        self.master_sound1 = mommy.make('Sound', phrase2='Once upon a midnight dreary')
        self.user_sound1 = mommy.make('Sound', phrase2='As I pondered, weak and weary')
        self.master_sound2 = mommy.make('Sound', phrase2='Over many a quaint and curious volume')
        self.user_sound2 = mommy.make('Sound', phrase2='of forgotten lore')
        self.pair1 = mommy.make('TestPair', master_sound=self.master_sound1, user_sound=self.user_sound1)
        self.pair2 = mommy.make('TestPair', master_sound=self.master_sound2, user_sound=self.user_sound2)
        self.test.pairs_to_test.add(self.pair1)
        self.test.pairs_to_test.add(self.pair2)

        mommy.make('TestConfig', associated_test=self.test, _quantity=5, date=timezone.now())
        mommy.make(
                'UserPhraseDatum', 
                score=.75, 
                result_from=self.config, 
                sounds=self.pair1,
                compare_time='.80085', _quantity=50
        )
        self.datum = mommy.make(
                'UserPhraseDatum', 
                score=.5,
                result_from=self.config,
                sounds=self.pair2,
                compare_time='.1'
        )

        self.word = mommy.make(
                'SegmentDatum',
                type_of_speech='WORD',
                score=.75,
                associated_datum=self.datum,
        )
        self.syllable1 = mommy.make(
                'SegmentDatum',
                text='kathrine has a kitty',
                type_of_speech='SYLLABLE',
                score=.5,
                associated_datum=self.datum,
                parent_segment=self.word
        )
        self.syllable2 = mommy.make(
                'SegmentDatum',
                text='bobby has a doggy',
                type_of_speech='SYLLABLE',
                score=.5,
                associated_datum=self.datum,
                parent_segment=self.word
        )
        mommy.make(
                'SegmentDatum',
                type_of_speech='PHONEME',
                text='bebe hes e dege',
                score=.25,
                parent_segment=self.syllable1,
                associated_datum=self.datum,
                _quantity=3
        )

        mommy.make(
                'SegmentDatum',
                type_of_speech='PHONEME',
                text='kethrn hes e kete',
                score=.25,
                parent_segment=self.syllable2,
                associated_datum=self.datum,
                _quantity=3
        )

        mommy.make(
                'SegmentDatum',
                type_of_speech='WORD',
                text='bebes deg',
                transcription='bb dg',
                raw_status='gud',
                master_start='0',
                master_end='1',
                user_start='2',
                user_end='3',
                score=.42,
                associated_datum=self.datum,
                _quantity=5
        )
        
        # User navigates to homepage
        self.browser.get("{}/soundtests".format(self.live_server_url))

        # User clicks "View Test Results" link and is brought to appropriate web-page
        self.browser.find_element_by_xpath('//a[contains(text(), "View Test Results")]').click()

        # User clicks the desired test ("Completed Test") and is brought to a list of runs for said test
        self.browser.find_element_by_xpath('//a[contains(text(), "Completed Test")]').click()

    def tearDown(self):
        self.browser.close()

    def test_view_list_of_tests_and_view_contents_of_particular_test(self):
        # User clicks the desired run and is brought to a list of results for said run
        self.browser.find_element_by_xpath('//a[contains(text(), "Desired Run")]').click()

        # User clicks a row and is brought to a list of associated segment data
        self.browser.find_element_by_xpath('//a[contains(text(), ".5")]').click()

from selenium import webdriver
from selenium.webdriver.support.ui import Select

from django.conf import settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

import os

class UploadSoundTest(StaticLiveServerTestCase):
    def setUp(self):
        # Set up browser
        geckodriver_path = os.path.join(settings.BASE_DIR, "tests", "geckodriver")
        self.browser = webdriver.Firefox(executable_path=geckodriver_path)

    def tearDown(self):
        self.browser.close()

    def test_upload_sound(self):
        # User navigates to homepage
        self.browser.get("{}/soundtests".format(self.live_server_url))

        # User clicks "Upload Sound" link and is brought to appropriate web-page
        self.browser.find_element_by_xpath("//a[contains(text(), 'Upload Sounds')]").click()

        # User fills out sound form with appropriate information
        phrase_1 = self.browser.find_element_by_id("id_phrase1")
        phrase_1.send_keys("User Sound") 
        phrase_2 = self.browser.find_element_by_id("id_phrase2")
        phrase_2.send_keys("User Sound") 
        language = self.browser.find_element_by_id("id_language")
        language.send_keys("ENGLISH")
        num_words = self.browser.find_element_by_id("id_num_words")
        num_words.send_keys("2")
        num_syllables = self.browser.find_element_by_id("id_num_syllables")
        num_syllables.send_keys("2")
        sex = Select(self.browser.find_element_by_id("id_sex"))
        sex.select_by_visible_text("Male")
        speaker_name = self.browser.find_element_by_id("id_speaker_name")
        speaker_name.send_keys("Geofferson Iversifon")

        # User clicks "Submit" 
        self.browser.find_element_by_xpath("//button[contains(text(), 'Submit File')]")

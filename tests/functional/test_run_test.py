from selenium import webdriver
from django.conf import settings
from django.test import LiveServerTestCase
from model_mommy import mommy

import os

class TestCreateTest(LiveServerTestCase):
    def setUp(self):
        # Set up browser
        geckodriver_path = os.path.join(settings.BASE_DIR, 'tests', 'geckodriver')
        self.browser = webdriver.Firefox(executable_path=geckodriver_path)

        # Create tests
        self.test1 = mommy.make_recipe('tests.sound_test')
        self.test2 = mommy.make_recipe('tests.sound_test')

    def tearDown(self):
        self.browser.close()

    def test_run_test(self):
        # Navigate to homepage
        self.browser.get('{}/soundtests'.format(self.live_server_url))

        # Click "Run Test/s" link
        self.browser.find_element_by_xpath('//a[contains(text(), "Run Test/s")]').click()

        # Check the box next to desired tests
        self.browser.find_element_by_xpath('//tr[td[contains(text(), "{}")]]//input'.format(self.test1.name)).click()
        self.browser.find_element_by_xpath('//tr[td[contains(text(), "{}")]]//input'.format(self.test2.name)).click()

        # Click the "Submit & Config" button
        self.browser.find_element_by_xpath('//button[contains(text(), "Submit & Config")]').click()


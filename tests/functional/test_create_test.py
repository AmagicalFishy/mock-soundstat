from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select, WebDriverWait

from django.conf import settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from model_mommy import mommy

from soundtests.models.sound import Sound
from soundtests.models.lesson import Lesson

import os
import random

class CreateNewTestTest(StaticLiveServerTestCase):
    def setUp(self):
        # Set up browser
        geckodriver_path = os.path.join(settings.BASE_DIR, "tests", "geckodriver")
        self.browser = webdriver.Firefox(executable_path=geckodriver_path)

        # Set up database w/ fake data
        self.lesson = mommy.make("Lesson")
        mommy.make("Sound", associated_lesson=self.lesson, _quantity=50)
        mommy.make(
                "Sound", 
                phrase1="Test Phrase!", 
                language="Test Language",
                associated_lesson=self.lesson,
                _quantity=5, 
        )
        self.languages = ['Language 1', 'Language 2', 'Language 3', 'Language 4']
        for language in self.languages:
            mommy.make('Sound', language=language, _quantity=10)
       
        # Set up browser ability to wait for javascript to finish
        self.wait = WebDriverWait(self.browser, 30)

    def tearDown(self):
        self.browser.close()
    
    def wait_for_ajax(self): # Helper function; tells browser to wait until AJAX is complete
        self.wait.until(lambda browser: self.browser.execute_script("return jQuery.active == 0"))

    def test_create_new_test(self):
        #self.browser.implicitly_wait(5)
        # Navigate to homepage
        self.browser.get("{}/soundtests".format(self.live_server_url))

        # Click "Create New Test" link and is brought to the appropriate page
        self.browser.find_element_by_xpath("//a[contains(text(), 'Create New Test')]").click()

        # Enter name and description for new test
        self.browser.find_element_by_id("id_form-0-name").send_keys("Test Test")
        self.browser.find_element_by_id("id_form-0-description").send_keys(
                "This is an in-depth and very detailed description of a test that will be created"
        )
        # Click on some filter (in this case, filter by number of syllables)
        select_num_syllables = self.browser.find_element_by_id('id_form-0-num_syllables')
        select = Select(select_num_syllables)
        random_option = random.choice(select.options)
        self.wait_for_ajax()
        random_option.click()

        # Click on a unique phrase
        select_unique_phrase = self.browser.find_element_by_id("id_form-0-unique_phrase")
        select = Select(select_unique_phrase)
        random.choice(select.options).click()

        # Click on the submit button, adding a master sound to the table
        self.browser.find_element_by_xpath("//button[contains(text(), 'Add Selected Sounds to Test')]").click()
        table = self.browser.find_elements_by_xpath("//table[@id='test_table']/tbody/tr")
        self.assertEquals(2, len(table))

        # Decide that sound shouldn't be in the test and removes it with the "X"
        self.browser.find_element_by_xpath("//a[contains(text(), 'X')]").click()
        table = self.browser.find_elements_by_xpath("//table[@id='test_table']/tbody/tr")
        self.assertEquals(1, len(table))

        # Add a few more sounds simultaneously
        random_option.click() # Remove filter
        select = Select(self.browser.find_element_by_id("id_form-0-unique_phrase"))
        ActionChains(self.browser).key_down(Keys.CONTROL).perform()
        for ii in range(15):
            random_option = random.choice(select.options)
            random_option.click()
        ActionChains(self.browser).key_up(Keys.CONTROL).perform()
        select = Select(self.browser.find_element_by_id('id_form-0-sounds_to_send'))
        options = select.options
            # Select all sounds
        ActionChains(self.browser).key_down(Keys.CONTROL).perform()
        for ii in range(0, len(options) - 1):
            select.select_by_index(ii)
        ActionChains(self.browser).key_up(Keys.CONTROL).perform()
            # Click submit to add them to the table
        self.browser.find_element_by_xpath("//button[contains(text(), 'Add Selected Sounds to Test')]").click()
        # Count number of sounds in table (for test purposes)
        num_sounds = len(self.browser.find_elements_by_xpath('//table[@id="test_table"]/tbody/tr'))
        

        # Click the submit button to go to associate master sounds w/ user sounds page
        self.browser.find_element_by_xpath("//button[contains(text(), 'Associate Sounds')]").click()

        # For every form/master sound..
        for ii in range(num_sounds - 1):
            # Pick one of the languages at random
            language_select = Select(self.browser.find_element_by_id('id_form-{}-language'.format(ii)))
            language_select.select_by_visible_text(random.choice(self.languages))

            # Select all 10 phrases of that language
            phrase_select = Select(self.browser.find_element_by_id('id_form-{}-unique_phrase'.format(ii)))
            ActionChains(self.browser).key_down(Keys.CONTROL).perform()
            for jj in range(0, 10):
                phrase_select.select_by_index(jj)

            # Select all 10 recordings of the above-selected phrases
            recording_select = Select(self.browser.find_element_by_id('id_form-{}-sounds_to_send'.format(ii)))
            for jj in range(0, 10):
                recording_select.select_by_index(jj)

        # Click submit button, get redirected to home page
        self.browser.find_element_by_xpath('//button[contains(text(), "Create Test")]').click()
        self.assertTemplateUsed('soundtests/index.html')

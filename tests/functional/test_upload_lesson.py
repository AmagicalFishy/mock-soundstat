from selenium import webdriver
from selenium.webdriver.support.ui import Select

from django.conf import settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

import os

class UploadLessonTest(StaticLiveServerTestCase):
    def setUp(self):
        # Set up browser
        geckodriver_path = os.path.join(settings.BASE_DIR, 'tests', 'geckodriver')
        self.browser = webdriver.Firefox(executable_path=geckodriver_path)
        self.path = os.path.abspath('tests/media/{}')

    def tearDown(self):
        self.browser.close()

    def test_upload_lesson(self):
        # Navigates to homepage
        self.browser.get('{}/soundtests'.format(self.live_server_url))

        # Clicks 'Upload Lesson' link and is brought to the appropriate page
        self.browser.find_element_by_xpath('//a[contains(text(), "Upload Lesson JSON")]').click()

        # Fills out lesson form with descriptive title
        lesson_title = self.browser.find_element_by_id('id_lesson_name')
        lesson_title.send_keys("Today's Descriptive Test Lesson")

        # Uploads lesson JSON
        lesson_file = self.browser.find_element_by_id('id_lesson_json_file')
        lesson_file.send_keys(self.path.format('test_upload_lesson_sounds.json'))

        # Clicks submit and is redirected to the lesson-sound classification page
        self.browser.find_element_by_xpath('//button[contains(text(), "Classify Sounds")]').click()

        # Fills out sounds with appropriate info
        self.browser.find_element_by_id('id_speaker_name').send_keys('Mr. Anderson') # Speaker Name
        self.browser.find_element_by_id('id_sex').send_keys('M') # Sex
      
        for ii in range(0,4):
            self.browser.find_element_by_id('id_form-{}-num_words'.format(ii)).send_keys(6)
            self.browser.find_element_by_id('id_form-{}-num_syllables'.format(ii)).send_keys(8)

        # Misses the last form (there are 5), clicks submit, and sees the error message
        self.browser.find_element_by_xpath('//button[@type="Submit"]').click()
        self.assertEquals('This field is required.', self.browser.find_element_by_class_name('errorlist').text)
        
        # Fills out field and is brought back to the home page
        self.browser.find_element_by_id('id_form-4-num_words').send_keys(7)
        self.browser.find_element_by_id('id_form-4-num_syllables').send_keys(7)





from django.conf import settings
from django.test import TestCase
from model_mommy import mommy
from unittest.mock import patch
from utils.builders import build_sound

import os
import pydub
import random
import requests
import sys
import wave

def mock_return_test_sound(url, verify):
    if url[-4:] == '.mp3':
        sound = open(os.path.join(settings.BASE_DIR, 'tests', 'media', 'dwarf.mp3'), 'rb')
    else:
        sound = open(os.path.join(settings.BASE_DIR, 'tests', 'media', 'cape_fear_audio.wav'), 'rb')
    class mocked_sound:
        content = sound.read()
    return mocked_sound()

class TestBuildSound(TestCase):
    def setUp(self):
        self.config = mommy.make(
                'TestConfig', 
                added_silence = True,
                silence_before = 5000,
                silence_after = 5000
        )
   
    # We have to access the module via a call to sys. Otherwise, trying to access
    # utils.builders.build_sound accesses the function directly (since a function
    # w/ the same name is imported in the __init__.py file of utils.builders)
    @patch.object(requests, 'get', side_effect=mock_return_test_sound)
    def test_build_sound_adds_silence_correctly(self, mocked_method):
        test_audio = wave.open(os.path.join(settings.BASE_DIR, 'tests', 'media', 'cape_fear_audio.wav'), 'r')
        original_duration = test_audio.getnframes()/float(test_audio.getframerate())
        generated_file = build_sound('i am a url', self.config)
        generated_audio = wave.open(generated_file.name, 'rb')
        generated_duration = generated_audio.getnframes()/float(generated_audio.getframerate())
        self.assertEquals(original_duration + 10, generated_duration)

    @patch.object(requests, 'get', side_effect=mock_return_test_sound)
    def test_build_sound_handles_urls_that_indicate_mp3_file(self, mocked_method):
        # We only need this to pass if ffmpeg doesn't throw an error trying to
        # open the mp3 file as a wav
        try:
            generated_file = build_sound('mp3 url.mp3', self.config)
            self.assertTrue(True)
        except pydub.exceptions.CouldntDecodeError:
            self.assertTrue(False, 'ffmpeg threw a CouldntDecodeError')

from django.test import TestCase
from model_mommy import mommy
from soundresults.models import SegmentDatum
from utils import random_gen
from utils.builders import build_segments

import random

def _random_dict(width=1, depth=1):
    """Helper function to generate random dictionaries that mimic server's segment json

    Args:
        width(int): Determines how many dictionaries should be at the top
        level (e.g. - If width=2, then the top-level list will have
        two segment dictionaries)

        depth (int): Determines how many dictionaries should be nested
        into the 'segments' entry (e.g. - If depth=3, then the top-level
        dictionary will have a dictionary as its 'segments' value, and that
        dictionary will also have a dictionary as its 'segments' value)

    """

    generated_list = []
    for ii in range(0, width):
        generated_list.append({
                'type': 'LEVEL {}'.format(depth),
                'score': random.randrange(0, 100)/100,
                'master_start': random.randrange(0, 100),
                'master_end': random.randrange(0, 100),
                'user_start': random.randrange(0, 100),
                'user_end': random.randrange(0, 100),
                'text': random.randrange(0, 100),
                'transcription': random_gen(),
                'status': random.randrange(0, 3),
                'raw_status': random_gen(),
                'segments': None
        })

        if depth != 1:
            generated_list[ii]['segments'] = _random_dict(width=width+1, depth=depth-1)

    return generated_list
    
class TestBuildDatum(TestCase):
    def setUp(self):
        self.datum = mommy.make('UserPhraseDatum')

    def test_build_segments_saves_all_segments_appropriately(self):
        segment_json = _random_dict(width=2, depth=3) # Results in 32 segments total
        build_segments(segment_json=segment_json, datum=self.datum)
        self.assertEquals(32, len(SegmentDatum.objects.all())) # Total segments

        # Top-level Segments
        top_level = SegmentDatum.objects.filter(parent_segment=None)
        self.assertEquals(2, len(top_level))

        # Mid-level Segments
        mid_level = SegmentDatum.objects.filter(parent_segment__in=top_level)
        self.assertEquals(6, len(mid_level))

        # Bottom-level Segments
        self.assertEquals(24, len(SegmentDatum.objects.filter(parent_segment__in=mid_level)))


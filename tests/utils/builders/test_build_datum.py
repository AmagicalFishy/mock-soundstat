from django.test import TestCase
from model_mommy import mommy
from soundresults.models import UserPhraseDatum
from utils.builders import build_datum

class TestBuildDatum(TestCase):
    def setUp(self):
        self.config = mommy.make('TestConfig')
        self.pair = mommy.make('TestPair')
        self.data_dict = {
                'score': 1,
                'snr': 2,
                'u_len': 3,
                'vad_start': 4,
                'vad_end': 5,
                'jsession_id': 'ABC123'
        }

    def test_build_datum_returns_appropriate_datum_instance(self):
        datum = build_datum(
                result_from=self.config,
                pair=self.pair,
                **self.data_dict
        )
        lookup_datum = UserPhraseDatum.objects.get(jsession_id=self.data_dict['jsession_id'])
        datum_dict = {}
        lookup_dict = {}
        for key in self.data_dict.keys():
            datum_dict[key] = getattr(datum, key)
            lookup_dict[key] = getattr(lookup_datum, key)

        self.assertDictEqual(self.data_dict, datum_dict)
        self.assertDictEqual(self.data_dict, lookup_dict)

    def test_build_datum_puts_default_values_in_datum(self):
        datum = build_datum(
                result_from=self.config,
                pair=self.pair,
                jsession_id='12345'
        )
        self.assertEqual(None, datum.score)
        self.assertEqual(None, datum.snr)
        self.assertEqual(None, datum.u_len)
        self.assertEqual(None, datum.vad_start)
        self.assertEqual(None, datum.vad_end)
        self.assertEqual('12345', datum.jsession_id)


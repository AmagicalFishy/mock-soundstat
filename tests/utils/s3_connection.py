from django.test import TestCase
from moto import mock_s3
from utils import S3Connection

import boto3

class TestS3Connection(TestCase):
    @classmethod
    def setUpClass(cls):
        """Sets up fake S3 Bucket"""
        cls.mock_s3 = mock_s3()
        cls.mock_s3.start()
        cls.s3 = boto3.Session(aws_access_key_id='fakeaccess', aws_secret_access_key='fakesecret')
        cls.s3.resource('s3').create_bucket(Bucket='test_bucket')
        cls.bucket = cls.s3.resource('s3').Bucket(name='test_bucket')
        # Upload something to the bucket
        cls.bucket.put_object(Key='floppy', Body='disk', ContentType='text/plain')

    @classmethod
    def tearDownClass(cls):
        cls.mock_s3.stop()

    def test_s3_connection_throws_error_when_no_keys_are_found(self):
        self.assertRaises(TypeError, S3Connection.__init__)

    def test_s3_connection_connects_appropriately(self):
        s3 = S3Connection(access_key='fakeaccess', secret_key='fakesecret', bucket_name='test_bucket')
        self.assertEquals(b'disk', s3.resource.Object('test_bucket', 'floppy').get()['Body'].read())

from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

class TestViewRuns(TestCase):
    def setUp(self):
        self.test = mommy.make('SoundTest')

    def test_view_runs_page_loads(self):
        response = self.client.get(reverse(
            'soundresults:view_runs', 
            kwargs={'test_slug': self.test.slug}
        ))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "soundresults/view_runs.html")

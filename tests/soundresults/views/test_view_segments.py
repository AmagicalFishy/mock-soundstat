from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

class TestViewSegments(TestCase):
    def setUp(self):
        self.test = mommy.make('SoundTest')
        self.config = mommy.make('TestConfig', associated_test=self.test)
        self.master_sound = mommy.make('Sound')
        self.user_sound = mommy.make('Sound')
        self.pair = mommy.make(
                'TestPair', 
                master_sound=self.master_sound, 
                user_sound=self.user_sound
        )
        self.datum = mommy.make(
                'UserPhraseDatum', 
                result_from=self.config,
                sounds=self.pair
        )

    def test_view_segments_page_loads(self):
        response = self.client.get(reverse(
            'soundresults:view_segments', 
            kwargs={
                'test_slug': self.test.slug,
                'config_pk': self.config.pk,
                'datum_pk': self.datum.pk
            }
        ))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "soundresults/view_segments.html")

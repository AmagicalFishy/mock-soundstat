from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

class TestViewResults(TestCase):
    def setUp(self):
        self.test = mommy.make('SoundTest')
        self.config = mommy.make('TestConfig', associated_test=self.test)

    def test_view_results_page_loads(self):
        response = self.client.get(reverse(
            'soundresults:view_results', 
            kwargs={
                'test_slug': self.test.slug,
                'config_pk': self.config.pk
            }
        ))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "soundresults/view_results.html")

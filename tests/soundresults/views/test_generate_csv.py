from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

import codecs
import csv

class TestViewRuns(TestCase):
    def setUp(self):
        # Prepare database (generating fake results)
        self.test = mommy.make('SoundTest')

        # Sounds
        self.master_sound1 = mommy.make('Sound', phrase2='master1')
        self.master_sound2 = mommy.make('Sound', phrase2='master2')
        self.user_sound1 = mommy.make('Sound', phrase2='user1')
        self.user_sound2 = mommy.make('Sound', phrase2='user2')

        # Test Pairs
        self.pair1 = mommy.make(
                'TestPair',
                master_sound=self.master_sound1,
                user_sound=self.user_sound1,
                associated_test = self.test
        )
        self.pair2 = mommy.make(
                'TestPair',
                master_sound=self.master_sound1,
                user_sound=self.user_sound2,
                associated_test = self.test
        )
        self.pair3 = mommy.make(
                'TestPair',
                master_sound=self.master_sound2,
                user_sound=self.user_sound1,
                associated_test = self.test
        )
        self.pair4 = mommy.make(
                'TestPair',
                master_sound=self.master_sound2,
                user_sound=self.user_sound2,
                associated_test = self.test
        )

        # Test Run
        self.config = mommy.make(
                'TestConfig', 
                associated_test=self.test,
                jsession_id='THIS IS THE POLICE'
        )

        # User Phrase Results
        self.data = []
        self.data.append(mommy.make(
                'UserPhraseDatum',
                sounds=self.pair1,
                result_from=self.config,
        ))
        self.data.append(mommy.make(
                'UserPhraseDatum',
                sounds=self.pair2,
                result_from=self.config,
        ))
        self.data.append(mommy.make(
                'UserPhraseDatum',
                sounds=self.pair3,
                result_from=self.config,
        ))
        self.data.append(mommy.make(
                'UserPhraseDatum',
                sounds=self.pair4,
                result_from=self.config,
        ))

        # Segments
        # This should result in a max of 1 word, 1 syllable, and 5 phonemes
        self.words = [] 
        for datum in self.data:
            mommy.make(
                    'SegmentDatum',
                    type_of_speech='PHONEME',
                    associated_datum=datum,
                    _quantity=5,
                    parent_segment=mommy.make(
                        'SegmentDatum',
                        type_of_speech='SYLLABLE',
                        associated_datum=datum,
                        parent_segment=mommy.make(
                            'SegmentDatum',
                            type_of_speech='WORD',
                            associated_datum=datum,
                        )
                    )
            )
            

    def test_generate_csv_appropriately_generates_csv(self):
        response = self.client.post(
                reverse('soundresults:generate_csv'),
                {
                    'runs[]': self.config.pk,
                    'download_csv': True
                }
        )
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        csv_reader = csv.reader(content.splitlines(), delimiter=',')

        # Check that header is what it's supposed to be
        headers = [
            'score', 
            'snr',
            'mlanguage', 
            'mphrase1', 
            'mphrase2', 
            'mspeaker', 
            'msex', 
            'msynthetic', 
            'mnum_words', 
            'mnum_syllables', 
            'ulanguage', 
            'uphrase1', 
            'uphrase2', 
            'uspeaker', 
            'usex', 
            'usynthetic', 
            'unum_words', 
            'unum_syllables', 
            'u_len',
            'added_silence', 
            'compare_time', 
            'server', 
            'server_version', 
            'date', 
            'test_name',
            'jsession_id',
            'WORD_1_status', 
            'WORD_1_score', 
            'WORD_1_tran', 
            'SYLLABLE_1_status', 
            'SYLLABLE_1_score', 
            'SYLLABLE_1_tran', 
            'PHONEME_1_status', 
            'PHONEME_1_score', 
            'PHONEME_1_tran', 
            'PHONEME_2_status', 
            'PHONEME_2_score', 
            'PHONEME_2_tran', 
            'PHONEME_3_status', 
            'PHONEME_3_score', 
            'PHONEME_3_tran', 
            'PHONEME_4_status', 
            'PHONEME_4_score', 
            'PHONEME_4_tran', 
            'PHONEME_5_status', 
            'PHONEME_5_score', 
            'PHONEME_5_tran', 
        ]
        self.assertListEqual(next(csv_reader), headers)

        # Check that all the phrase2 are what they're supposed to be
        # and are in the right order (if these two specifically are in the
        # correct order, then so is everything else... probably)
        results = [
                ['master2', 'user2'],
                ['master2', 'user1'],
                ['master1', 'user2'],
                ['master1', 'user1']
        ]
        for row in csv_reader:
            self.assertListEqual(
                    [row[4], row[12]], 
                    results.pop()
            )

    def test_generate_csv_returns_error_if_no_test_is_selected(self):
        response = self.client.post(
                reverse('soundresults:generate_csv'),
                {
                    'download_csv': True
                }
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("YOU NEED TO SELECT AT LEAST ONE RUN", str(response.content))


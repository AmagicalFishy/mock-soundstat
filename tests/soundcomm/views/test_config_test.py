from django.shortcuts import reverse
from django.test import TestCase
from model_mommy import mommy
from soundcomm.views import config_test
from soundtests.models import TestConfig
from unittest.mock import patch

import sys

class TestSelectTest(TestCase):
    def setUp(self):
        # Create Sound Tests
        self.test1 = mommy.make_recipe('tests.sound_test')
        self.test2 = mommy.make_recipe('tests.sound_test')

        # Create URL
        self.url = '{}?selected_tests={}selected_tests={}'.format(
                reverse('soundcomm:config_test'),
                self.test1.name,
                self.test2.name
        )
        
        # Set up data
        self.data = {
                'form-TOTAL_FORMS': '2',
                'form-INITIAL_FORMS': '2',
                'form-MAX_NUM_FORMS': '2',
                'form-0-associated_test': self.test1.name,
                'form-0-gain': 0,
                'form-0-vad_start': 0,
                'form-0-user_sound_snr': 25,
                'form-0-server': TestConfig.SERVER_CHOICES[0][0],
                'form-0-added_silence': False,
                'form-0-silence_before': 1,
                'form-0-silence_after': 1,
                'form-0-random_snr': False,
                'form-1-associated_test': self.test2.name,
                'form-1-gain': 0,
                'form-1-vad_start': 0,
                'form-1-user_sound_snr': 25,
                'form-1-server': TestConfig.SERVER_CHOICES[0][0],
                'form-1-added_silence': False,
                'form-1-silence_before': 1,
                'form-1-silence_after': 1,
                'form-1-random_snr': False
        }


    def test_config_test_loads(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'soundcomm/config_test.html')
   
    # We have to access the module via a call to sys. Otherwise, trying to access
    # soundcomm.views.config_test accesses the function directly (since a
    # function w/ the same name is imported in the __init__.py file of 
    # soundcom.views)
    @patch.object(sys.modules['soundcomm.views.config_test'], 'chain')
    def test_config_test_redirects_to_loading_page_on_successful_submit(self, mocked_method):
        response = self.client.post(self.url, data=self.data)
        self.assertRedirects(response, reverse('soundcomm:loading_page'))

    def test_config_returns_to_formset_page_on_submission_of_invalid_data(self):
        invalid_data = self.data.copy()
        invalid_data['form-0-server'] = "THIS IS NOT A SERVER CHOICE"
        response = self.client.post(self.url, data=invalid_data)
        self.assertTemplateUsed(response, 'soundcomm/config_test.html')

    def test_config_returns_to_formset_page_on_weird_request_method(self):
        response = self.client.put(self.url, data=self.data)
        self.assertRedirects(response, reverse('soundcomm:config_test'))














from django.shortcuts import reverse
from django.test import TestCase
from soundcomm.views import select_test

class TestSelectTest(TestCase):
    def test_select_test_loads(self):
        response = self.client.get(reverse('soundcomm:select_test'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'soundcomm/select_test.html')

    def test_select_test_redirects_to_homepage_on_non_get_request(self):
        response = self.client.post(reverse('soundcomm:select_test'))
        self.assertRedirects(response, reverse('soundtests:index'))

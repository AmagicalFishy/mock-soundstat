from django.test import TestCase
from django.utils import timezone
from soundcomm.forms import ConfigForm
from soundtests.models import TestConfig
from model_mommy import mommy

import random

class TestConfigForm(TestCase):
    def setUp(self):
        # Create dummy test
        self.test = mommy.make_recipe('tests.sound_test')

        # Create data
        self.data = {
                'associated_test': self.test.name,
                'gain': 0,
                'vad_start': 0,
                'user_sound_snr': 25,
                'server': TestConfig.SERVER_CHOICES[random.randrange(0,2)][0],
                'scoring': TestConfig.SCORING_CHOICES[random.randrange(0,1)][0],
                'added_silence': True,
                'silence_before': random.randrange(0, 100),
                'silence_after': random.randrange(0, 100),
                'random_snr': False
        }
        self.no_silence_data = self.data
        self.no_silence_data['added_silence'] = False


    def test_config_form_validates_with_appropriate_data(self):
        # Fill form w/ good data
        form = ConfigForm(data=self.data)
        self.assertTrue(form.is_valid())

    def test_config_form_does_not_validate_with_duplicated_sound_and_no_added_silence(self):
        # Create test pairs which duplicate user sound
        ms1 = mommy.make('Sound')
        ms2 = mommy.make('Sound')
        us = mommy.make('Sound')

        test_pair_1 = mommy.make('TestPair', master_sound=ms1, user_sound=us)
        test_pair_2 = mommy.make('TestPair', master_sound=ms2, user_sound=us)
        self.test.pairs_to_test.add(test_pair_1)
        self.test.pairs_to_test.add(test_pair_2)
        self.test.save()
        
        # Fill form w/ data w/o added_silence
        form = ConfigForm(data=self.no_silence_data)
        form.is_valid()
        self.assertIn("This test contains multiple instances of the same user sound", form.errors['__all__'][0])

    def test_config_form_does_not_validate_with_sound_sent_in_past_four_hours(self):
        # Create TestConfig that was run 2 hours prior w/ same sound
        previous_test_run = mommy.make(
                'TestConfig', 
                associated_test=self.test,
                added_silence=False,
                server=self.data['server'],
                date = timezone.now() - timezone.timedelta(hours=2)
        )
        previous_test_run.save()
        
        form = ConfigForm(data=self.no_silence_data)
        form.is_valid()
        self.assertIn("This test contains a user sound identical to one that has", form.errors['__all__'][0])

    def test_config_form_appropriately_handles_submission_of_invalid_server_choice(self):
        invalid_server_data = self.data.copy()
        invalid_server_data['server'] = "THIS IS NOT A SERVER CHOICE"
        form = ConfigForm(data=invalid_server_data)
        self.assertIn("Invalid server choice", form.errors['__all__'][0])











from django.forms import formset_factory
from django.test import TestCase
from model_mommy import mommy
from soundcomm.forms import ConfigBaseFormSet, ConfigForm
from soundcomm.forms.config_base_formset import find_common_hash
from soundtests.models import TestConfig

import random

class TestFindCommonHash(TestCase):
    def setUp(self):
        self.list1 = ['Animals 1', ['toad', 'frog']]
        self.list2 = ['Animals 2', ['bat', 'rat', 'monkey', 'pig']]
        self.list3 = ['Animals 3', ['snake', 'human', 'gopher', 'frog']]
        self.list4 = ['Animals 4', ['turkey', 'pigeon', 'horse']]
        self.list5 = ['Animals 5', ['cat']]

    def test_find_common_hash_helper_function_returns_lists_on_list_entry_duplicate(self):
        intersection = find_common_hash([self.list1, self.list2, self.list3, self.list4, self.list5])
        self.assertEquals('Animals 3 - Animals 1', '{} - {}'.format(intersection[0][0], intersection[1][0]))

    def test_find_common_hash_helper_function_returns_false_with_disjoint_lists(self):
        self.assertFalse(find_common_hash([self.list1, self.list2, self.list4, self.list5]))

    def test_find_common_hash_helper_function_returns_false_with_single_list(self):
        self.assertFalse(find_common_hash([self.list1])) 
        self.assertFalse(find_common_hash([self.list2]))
        self.assertFalse(find_common_hash([self.list3]))

class TestConfigBaseFormSet(TestCase):
    def setUp(self):
        # Set up fake tests
        self.test1 = mommy.make('SoundTest')
        self.test2 = mommy.make('SoundTest')
        self.test3 = mommy.make('SoundTest')

        # Duplicate pairs for tests 2 and 3
        duplicated_sound = mommy.make('Sound')
        pair1 = mommy.make('TestPair', master_sound=mommy.make('Sound'), user_sound=duplicated_sound)
        pair2 = mommy.make('TestPair', master_sound=mommy.make('Sound'), user_sound=duplicated_sound)

        self.test1.pairs_to_test.add(pair1)
        self.test3.pairs_to_test.add(pair2)

        self.test1.save()
        self.test3.save()

        # Create data
        self.data = {
                'form-TOTAL_FORMS': '2',
                'form-INITIAL_FORMS': '2',
                'form-MAX_NUM_FORMS': '2',
                'form-0-associated_test': self.test1.name,
                'form-0-gain': 0,
                'form-0-vad_start': 0,
                'form-0-user_sound_snr': 25,
                'form-0-server': TestConfig.SERVER_CHOICES[0][0],
                'form-0-added_silence': False,
                'form-0-silence_before': 1,
                'form-0-silence_after': 1,
                'form-0-random_snr': False,
                'form-1-associated_test': self.test2.name,
                'form-1-gain': 0,
                'form-1-vad_start': 0,
                'form-1-user_sound_snr': 25,
                'form-1-server': TestConfig.SERVER_CHOICES[0][0],
                'form-1-added_silence': False,
                'form-1-silence_before': 1,
                'form-1-silence_after': 1,
                'form-1-random_snr': False
        }

        self.ConfigBaseFormSet_factory = formset_factory(
                form=ConfigForm,
                formset=ConfigBaseFormSet,
                extra=0
        )
    
    def test_config_base_formset_validates_with_data(self):
        formset = self.ConfigBaseFormSet_factory(data=self.data)
        self.assertTrue(formset.is_valid())

    def test_config_base_formset_does_not_validate_with_duplicated_sounds(self):
        duplicate_data = self.data.copy()
        duplicate_data['form-1-associated_test'] = self.test3.name
        formset = self.ConfigBaseFormSet_factory(data=duplicate_data)
        formset.is_valid()
        self.assertIn("They each contain an identical user sound", formset.non_form_errors()[0])

    def test_config_base_formset_handles_key_error_upon_one_of_its_forms_having_an_invalid_server(self):
        invalid_server = self.data.copy()
        invalid_server['form-1-server'] = "THIS IS NOT A VALID SERVER CHOICE"
        formset = self.ConfigBaseFormSet_factory(data=invalid_server)
        formset.is_valid()
        self.assertIn("Invalid server choice", formset.errors[1]['__all__'])

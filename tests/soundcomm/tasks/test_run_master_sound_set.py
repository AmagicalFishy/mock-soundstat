from django.test import TestCase
from model_mommy import mommy
from soundcomm.tasks import run_master_sound_set
from soundtests.models import Sound
from unittest.mock import patch

import json
import sys

# Fake functions
class MockedServerComm:
    """Pretends to be a ServerComm instance

    This allows us to test the run_master_sound_set function w/o actually connecting
    to the server. most of the methods used in run_master_sound_set come from other
    files/functions, so most of the methods here are mocked.

    """
    
    def __init__(self, serialized_instance):
        self.version = 'SERVERTESTVERSION1.0XXXX' 
        self.jsessionid = '12345678901234567890123456789012'
        self.last_response_time = '4.1234'

    def set_lesson(self, lesson):
        pass

    def compare_sounds(self, user_sound, master_sound):
        fake_response = {
                "compareResult":[0.42],
                "segments": [{
                    "type": "Blood Type",
                    "score": 0,
                    "master_start": 0,
                    "master_end": 10,
                    "user_start": 0,
                    "user_end": 10,
                    "text": "Except while driving",
                    "transcription": "X-sept wyle dr-eye-ving",
                    "status": 2,
                    "raw_status": "Rare",
                    "segments": None
                }],
                "userDetails": {"vad": {"start": 0.11, "end": 0.76}}
        }

        class Dummy: 
            text=json.dumps(fake_response)
        return Dummy

def mocked_build_sound(url, config):
    class sound:
        name = 'i am a sound lol'
    return sound

def mocked_build_datum(result_from, pair, **dictionary):
    pass

def mocked_build_segments(segment_json, datum):
    pass

class mocked_wave:
    class FakeSound:
        def getnframes(self):
            return 10
        def getframerate(self):
            return 2

    def open(self, name, read):
        return FakeSound

class mocked_datum(list):
    def __init__(self):
        self.append(':l')

    class fake_datum:
        def update(
                self, 
                snr=None, 
                score=None, 
                jsessionid=None, 
                ulen=None, 
                vad_start=None, 
                vad_end=None, 
                compare_time=None
        ):
            pass

    class objects:
        def filter(self):
            return fake_datum

class TestRunTest(TestCase):
    def setUp(self):
        """Sets up fake lessons, sounds, tests, etc."""
        self.lesson = mommy.make('Lesson')
        self.master_sound = mommy.make('Sound', associated_lesson=self.lesson)
        self.user_sound = mommy.make('Sound')
        self.test_pair = mommy.make('TestPair', master_sound=self.master_sound, user_sound=self.user_sound)
        self.test = mommy.make('SoundTest')
        self.test.associated_lessons.add(self.lesson)
        self.test.pairs_to_test.add(self.test_pair)
        self.config = mommy.make('TestConfig', associated_test=self.test)
        self.datum = mommy.make('UserPhraseDatum', result_from=self.config, sounds=self.test_pair)

    @patch.object(sys.modules['soundcomm.tasks.run_master_sound_set'], 'ServerComm', side_effect=MockedServerComm)    
    @patch.object(sys.modules['soundcomm.tasks.run_master_sound_set'], 'build_segments', side_effect=mocked_build_segments)
    @patch.object(sys.modules['soundcomm.tasks.run_master_sound_set'], 'build_datum', side_effect=mocked_build_datum)
    @patch.object(sys.modules['soundcomm.tasks.run_master_sound_set'], 'build_sound', side_effect=mocked_build_sound)
    @patch.object(sys.modules['soundcomm.tasks.run_master_sound_set'], 'wave', side_effect=mocked_wave)
    @patch.object(sys.modules['soundcomm.tasks.run_master_sound_set'], 'UserPhraseDatum', side_effect=mocked_datum)
    def test_run_master_sound_set_runs(self, mocked_srvcm, mocked_sgmt, mocked_dt, mocked_snd, mocked_wv, mocked_dtm):
        run_master_sound_set(
                init_data={'server_comm': 'blah blah blah', 'config_pk': self.config.pk},
                sound_pk=self.master_sound.pk
        )



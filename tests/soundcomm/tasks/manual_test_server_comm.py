from django.conf import settings
from django.test import TestCase
from model_mommy import mommy
from soundcomm.tasks.server_comm import ServerComm
from soundtests.models import Sound, TestConfig
from utils.builders import build_sound

import json
import requests
import wave

class TestServerCommTest(TestCase):
    """
    This test serves mostly to test the respone from the server. The class
    itself does not have much business-logic, and is basically a book keeping
    class

    One should run this test manually; since it's dependent on a 3rd party API,
    and Bamboo runs tests in order to build the project, the Bamboo build may
    fail if the API doesn't work (i.e. - it may fail to build even though there
    is nothing wrong in the code)

    """

    def setUp(self):
        """Set up fleshed out lesson and sound instances

        We use the user-created lesson entitled "QASA Test Lesson" by the user
        "jtimberlake" on the Test server which, as of 2019.03.08, is located
        at http://192.168.254.77/jsndcmp

        """

        self.server = TestConfig.SERVER_CHOICES[1][0] # Test Server
        self.lesson_json = json.load(open('{}/tests/media/QASA_test_lesson.json'.format(settings.BASE_DIR)))
        self.server_comm = ServerComm(self.server) 

        # Make lesson instance
        self.lesson = mommy.make(
                'Lesson',
                lesson_name = 'Test Lesson',
                json_name = self.lesson_json['listName'],
                activity_type = 'pron',
                learning_language = self.lesson_json['languageLearn'], 
                known_language = self.lesson_json['languageNative']
        )
        # Make sound instances; associate w/ lesson
        for sound in self.lesson_json['files']:
            new_sound = mommy.make(
                    'Sound',
                    phrase1=sound['phrase1'],
                    phrase2=sound['phrase2'],
                    lesson_url=sound['url'],
                    lesson_hash=sound['hash'],
                    associated_lesson=self.lesson
            )
            self.lesson.associated_sounds.add(new_sound)

    def test_set_jsession_id_headers_and_urls_are_set_appropriately(self):
        self.assertEquals(len(self.server_comm.jsessionid), 32)
        self.assertEquals(
                self.server_comm.serverlist_url,
                '{}/v2/sound/serverlist;jsessionid={}'.format(self.server, self.server_comm.jsessionid)
        )
        self.assertEquals(
                self.server_comm.compare_url,
                '{}/v2/sound/compare;jsessionid={}'.format(self.server, self.server_comm.jsessionid)
        )

    def test_set_lesson_returns_200_status_code_with_no_errors(self):
        response = self.server_comm.set_lesson(self.lesson)
        self.assertEquals(200, response.status_code)
        self.assertIsNone(json.loads(response.text)['errorStatus'])
        self.assertIsNone(json.loads(response.text)['errorMessage'])

    def test_compare_sounds_returns_well_formatted_json_dict(self):
        """
        If this test fails, do not first change the test to make it succeed. 
        First, ensure that any other code which uses these values is updated.
        THEN change these tests. 
        
        The purpose of this test is to make sure the server returns the JSON
        that we expect. Since this the returned JSON is open to change, we 
        may have to change the code according to what the server returns.

        """
        response = self.server_comm.set_lesson(self.lesson) # Set lesson

        # Compare the "Fourth phrase" sound
        master_sound = Sound.objects.get(phrase1='Fourth phrase')
        user_sound = open('{}/tests/media/fourth_phrase.wav'.format(settings.BASE_DIR), 'rb')
        response = self.server_comm.compare_sounds(user_sound, master_sound)
        response_json = json.loads(response.text)

        # Ensure first-level keys are what we expect them to be
        response_keys = [
                'errorStatus', 'errorMessage', 'compareResult', 'masterMaxSampleIndexes',
                'masterDetails', 'masterVADResults', 'segments', 'userMaxSampleIndex', 
                'userDetails', 'userVadResult', 'userSoundKey', 'isCancelled'
        ]
        self.assertCountEqual(response_keys, response_json.keys())

        # Ensure segment keys are what we expect them to be
        segment_keys = [
                'type', 'score', 'masterStart', 'masterEnd', 'userStart',
                'userEnd', 'text', 'transcription', 'status', 'segments',
                'rawStatus'
        ]
        self.assertCountEqual(segment_keys, response_json['segments'][0].keys())

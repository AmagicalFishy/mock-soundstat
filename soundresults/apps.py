from django.apps import AppConfig


class SoundresultsConfig(AppConfig):
    name = 'soundresults'

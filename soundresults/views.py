# -*- coding: utf-8 -*-

import csv
import io
import tempfile
import shutil
import logging
import json

from datetime import datetime
from numpy import mean, median, std

from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import render, redirect

from soundtests.models.testmodels import SoundTest, TestConfig
from soundresults.models.datamodels import PartOfSpeech
from soundresults.forms import TestFilterForm

logger = logging.getLogger('standard_logger')

def tests(request):
    """
    View that displays all available tests
    """
    form = TestFilterForm()
    tests = SoundTest.objects.all()
    return render(request, 'soundresults/viewtests.html', {'tests':tests, 'form':form})


def runs(request, test_key):
    """
    View that displays all the runs for an available test
    """

    test = SoundTest.objects.get(pk=test_key)
    runs = test.runs.all()
            
    return render(request, 'soundresults/viewruns.html', {'name': test.name, 'runs': runs})


def results(request, test_key, run_key):
    """
    View that displays a table of results for a particular test run
    """

    test = SoundTest.objects.get(pk=test_key)
    test_run = test.runs.get(pk=run_key)
   
    score_list = []
    for datum in test_run.data.all():
        score_list.append(datum.score)

    return render(
            request, 
            'soundresults/viewresults.html', 
            {
                'name': test.name, 
                'data': test_run.data.all(),
                'score_mean': round(mean(score_list), 3),
                'score_median': round(median(score_list), 3),
                'score_std': round(std(score_list), 3)
            }
    )

def generate_csv(request):
    """
    This view doesn't actually display anything, but generates a CSV for the "Download CSV" 
    button
    """

    if request.method == 'POST':
        logger.info("Generating CSV...")
        try:
            results = TestConfig.objects.filter(pk__in = request.POST.getlist('runs[]'))
        except ValueError:
            results = TestConfig.objects.filter(jsession_id__in = request.POST.getlist('runs[]'))
        
        segments = PartOfSpeech.objects.filter(associated_datum__result_from__in=results)
        
        # Get the maximum number of words, syllables and phonemes of a phrase (to size CSV header appropriately)
        max_dict = {'WORD':0, 'SYLLABLE':0, 'PHONEME':0}
        for key in max_dict.keys():
            max_dict[key] = max([
                k['num'] for k in segments.filter(type_of_speech=key).values('associated_datum').annotate(num=Count('pk'))
            ])
        
        logger.info(request.POST.getlist('runs[]'))
        csv_buffer = io.StringIO()
        data_writer = csv.writer(csv_buffer)

        # Write title row
        logger.info("Constructing & writing header row...")
        # Construct header row
        row = [
            "score", "snr",
            "mlanguage", "mphrase1", "mphrase2", "mspeaker", "msex", "msynthetic", "mnum_words", 
            "mnum_syllables", "mstarts_with", 
            "ulanguage", "uphrase1", "uphrase2", "uspeaker", "usex", "usynthetic", "unum_words", 
            "unum_syllables", "ustarts_with", "ulen",
            "added_silence", "compare_time", "server", "server_version", "date", "test_name",
            "jsession_id"
        ]
        for key in max_dict.keys():
            for ii in range(1, max_dict[key] + 1):
                row.append('{}_{}_{}'.format(key, ii, 'status')) # i.e. - 'SYLLABLE_3_status'
                row.append('{}_{}_{}'.format(key, ii, 'score')) # i.e. - 'WORD_2_score'
                row.append('{}_{}_{}'.format(key, ii, 'tran')) # i.e. - 'PHONEME_6_tran'
        
        data_writer.writerow(row)
        for result in results: # For every result the user selected
            for datum in result.data.all(): #For every datum in the results data
                row = [
                        datum.score,
                        datum.snr,
                        datum.sounds.master_sound.language,
                        datum.sounds.master_sound.phrase1,
                        datum.sounds.master_sound.phrase2,
                        datum.sounds.master_sound.speaker_name,
                        datum.sounds.master_sound.sex,
                        datum.sounds.master_sound.voice,
                        datum.sounds.master_sound.num_words,
                        datum.sounds.master_sound.num_syllables,
                        datum.sounds.master_sound.starts_with,
                        datum.sounds.user_sound.language,
                        datum.sounds.user_sound.phrase1,
                        datum.sounds.user_sound.phrase2,
                        datum.sounds.user_sound.speaker_name,
                        datum.sounds.user_sound.sex,
                        datum.sounds.user_sound.voice,
                        datum.sounds.user_sound.num_words,
                        datum.sounds.user_sound.num_syllables,
                        datum.sounds.user_sound.starts_with,
                        datum.u_length,
                        result.added_silence,
                        datum.compare_time,
                        result.server,
                        result.server_version,
                        result.date,
                        result.associated_test.name,
                        result.jsession_id
                ]

                for key in max_dict.keys():
                    segments = datum.segments.filter(type_of_speech=key) # All segments of type 'key' (i.e. - WORD)
                    for segment in reversed(segments):
                        row.append(segment.raw_status) # Add status to row
                        row.append(segment.score) # Add score to row
                        row.append(segment.transcription) # Add transcription to row
                    # If there are fewer segments than the max-number, make sure these
                    # cells are empty
                    for ii in range(max_dict[key] - len(segments)):
                        row.append('')
                        row.append('')
                        row.append('')
                data_writer.writerow(row)

        csv_buffer.seek(0)
        if not request.POST.get("runs[]"):
            return HttpResponse("YOU NEED TO SELECT AT LEAST ONE RUN. NOW USE THE BACK BUTTON >:(")

        if request.POST.get("download_csv"):
            logger.info("Serving CSV to client...")
            filename = request.POST.get("test_name") + "-" + datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + ".csv"
            response = HttpResponse(csv_buffer, content_type='application/x-download')
            response['Content-Disposition'] = 'attachment;filename="' + filename + '"'
            return response

        elif request.POST.get("explore_data"):
            logger.info("Creating TMP CSV File for Shiny's use...")
            tmp_csv_file = tempfile.NamedTemporaryFile(mode='w', dir='./tmp_data', delete=False, encoding='utf-8')
            shutil.copyfileobj(csv_buffer, tmp_csv_file)
            tmp_csv_file.seek(0)
            shutil.chown(tmp_csv_file.name, user=999, group=999)
            shiny_url = '/soundshine?tmp_file_name=' + tmp_csv_file.name.replace('/soundstat/tmp_data/','')
            logger.info("Tmp. File name: " + tmp_csv_file.name + " - " + "Redirected URL: " + shiny_url)
            return redirect(shiny_url)

def filter_tests(request):
    """
    This view's sole purpose is to deal with the AJAX requests that are sent by the
    TestFilterForm. To add another filter, simply add another if clause w/ the appropiate
    list sent when the user clicks on a particular attribute in the form
    """

    # Checks for AJAX call - When user clicks one of the 3 filters, return
    # the list of unique phrases that match these filters
    if request.is_ajax():
        configs = TestConfig.objects.all()
        search_values = list(request.GET.keys())
        if 'test_name[]' in search_values:
            tests = SoundTest.objects.filter(name__in = request.GET.getlist('test_name[]'))
            configs = configs.filter(associated_test__in = tests.all())

        if 'server_version[]' in search_values:
            configs = configs.filter(server_version__in = request.GET.getlist('server_version[]'))

        if 'date_start' in search_values:
            try:
                configs = configs.filter(date__gte=datetime.strptime(request.GET.get('date_start'), '%m/%d/%Y'))
            except ValueError:
                pass

        if 'date_end' in search_values:
            try:
                configs = configs.filter(date__lte = datetime.strptime(request.GET.get('date_end'), '%m/%d/%Y'))
            except ValueError:
                pass
        configs = configs.filter(date__isnull=False)
        configs = sorted(configs, key=lambda x: x.date)
        jsession_ids = [{'id':x.jsession_id, 'date':x.date.strftime('%m/%d/%Y - %H:%M'), 'test':x.associated_test.name} for x in configs]
        return HttpResponse(json.dumps(jsession_ids))


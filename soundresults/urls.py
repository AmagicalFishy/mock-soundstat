from django.urls import path
from soundresults import views

app_name = 'soundresults'

urlpatterns = [
        path('view_tests', views.ViewTests.as_view(), name='view_tests'),
        path('view_tests/<test_slug>', views.ViewRuns.as_view(), name='view_runs'),
        path('view_tests/<test_slug>/<config_pk>', views.ViewResults.as_view(), name='view_results'),
        path('view_tests/<test_slug>/<config_pk>/<datum_pk>', views.ViewSegments.as_view(), name='view_segments'),
        path('generate_csv', views.generate_csv, name='generate_csv'),
]

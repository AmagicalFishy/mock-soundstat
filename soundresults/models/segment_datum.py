from django.db import models
from soundresults.models import UserPhraseDatum
        
class SegmentDatum(models.Model):
    type_of_speech = models.CharField(max_length=10, null=True, editable=False)
    score = models.DecimalField(max_digits=3, decimal_places=2, null=True, editable=False)
    master_start = models.DecimalField(max_digits=5, decimal_places=4, null=True, editable=False)
    master_end = models.DecimalField(max_digits=5, decimal_places=4, null=True, editable=False)
    user_start = models.DecimalField(max_digits=5, decimal_places=4, null=True, editable=False)
    user_end = models.DecimalField(max_digits=5, decimal_places=4, null=True, editable=False)
    text = models.CharField(max_length=50, null=True, editable=False)
    transcription = models.CharField(max_length=50, null=True, editable=False)
    status = models.PositiveIntegerField(null=True, editable=False)
    raw_status = models.CharField(max_length=20, null=True, editable=False)
    associated_datum = models.ForeignKey(UserPhraseDatum, on_delete=models.CASCADE, related_name='segments', null=True, editable=False)
    parent_segment = models.ForeignKey('self', on_delete=models.CASCADE, related_name='segments', null=True, default=None, editable=False)

    class Meta:
        db_table = 'segment_data'


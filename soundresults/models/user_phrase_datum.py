from django.db import models
from soundtests.models import TestConfig, TestPair

class UserPhraseDatum(models.Model):
    """This can be thought of as a row in a CSV file

    This contains all of the information returned from the server when a user
    sound is compared to a master sound

    """
    
    result_from = models.ForeignKey(TestConfig, on_delete=models.SET_NULL, related_name="data", null=True)
    sounds = models.ForeignKey(TestPair, on_delete=models.SET_NULL, related_name="result_data", null=True)
    score = models.DecimalField(max_digits=5, decimal_places=4, null=True)
    snr = models.PositiveSmallIntegerField(null=True)
    m_len = models.PositiveIntegerField(null=True, default=None)
    u_len = models.PositiveIntegerField(null=True, default=None)
    vad_start = models.DecimalField(max_digits=4, decimal_places=3, null=True, default=None)
    vad_end = models.DecimalField(max_digits=4, decimal_places=3, null=True, default=None)
    compare_time = models.DecimalField(max_digits=4, decimal_places=2, null=True, default=None)
    jsession_id = models.CharField(max_length=32)

    def __str__(self):
        return self.sounds.user_sound.phrase1 + " -> " + self.sounds.master_sound.phrase1 + ": " + str(self.score)

    class Meta:
        db_table = 'user_phrase_data'
        ordering = ['sounds']


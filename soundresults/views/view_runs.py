from django.views.generic import ListView
from soundtests.models import SoundTest, TestConfig

class ViewRuns(ListView):
    """Lists all available runs for a particular test"""
    context_object_name = 'runs'
    model = TestConfig
    paginate_by = 25
    slug_url_kwarg = 'test_slug'
    template_name = 'soundresults/view_runs.html'

    def get_queryset(self):
        associated_test = SoundTest.objects.get(slug=self.kwargs.pop('test_slug'))
        return TestConfig.objects.filter(associated_test=associated_test)

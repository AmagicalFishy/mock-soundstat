from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone
from soundtests.models import TestConfig
from soundresults.models import SegmentDatum

import csv
import io
import shutil
import tempfile

def generate_csv(request):
    """Generates a CSV of selected test runs
    
    This view doesn't display anything, but generates a CSV which is either
    downloaded by the user or sent to the Shiny container (as of now, CSV
    is the easiest way to communicate between Django and Shiny)

    """
    
    if request.method == 'POST':
        if request.POST.get('runs[]'):
            results = TestConfig.objects.filter(pk__in = request.POST.getlist('runs[]'))
            segments = SegmentDatum.objects.filter(associated_datum__result_from__in=results)
            
            # Get the maximum number of words, syllables and phonemes of a phrase (to size CSV header appropriately)
            max_dict = {'WORD':0, 'SYLLABLE':0, 'PHONEME':0}
            for key in max_dict.keys():
                max_dict[key] = max([
                    k['num'] for k in segments.filter(type_of_speech=key).values('associated_datum').annotate(num=Count('pk'))
                ])
            
            csv_buffer = io.StringIO()
            data_writer = csv.writer(csv_buffer)

            # Construct header row
            header_row = [
                'score', 
                'snr',
                'mlanguage', 
                'mphrase1', 
                'mphrase2', 
                'mspeaker', 
                'msex', 
                'msynthetic', 
                'mnum_words', 
                'mnum_syllables', 
                'ulanguage', 
                'uphrase1', 
                'uphrase2', 
                'uspeaker', 
                'usex', 
                'usynthetic', 
                'unum_words', 
                'unum_syllables', 
                'u_len',
                'added_silence', 
                'compare_time', 
                'server', 
                'server_version', 
                'date', 
                'test_name',
                'jsession_id'
            ]

            for key in max_dict.keys():
                for ii in range(1, max_dict[key] + 1):
                    header_row.append('{}_{}_{}'.format(key, ii, 'status')) # i.e. - 'SYLLABLE_3_status'
                    header_row.append('{}_{}_{}'.format(key, ii, 'score')) # i.e. - 'WORD_2_score'
                    header_row.append('{}_{}_{}'.format(key, ii, 'tran')) # i.e. - 'PHONEME_6_tran'
            
            data_writer.writerow(header_row)
            for result in results: # For every result the user selected
                for datum in result.data.all(): # For all data in the results data
                    row = [
                            datum.score,
                            datum.snr,
                            datum.sounds.master_sound.language,
                            datum.sounds.master_sound.phrase1,
                            datum.sounds.master_sound.phrase2,
                            datum.sounds.master_sound.speaker_name,
                            datum.sounds.master_sound.sex,
                            datum.sounds.master_sound.voice,
                            datum.sounds.master_sound.num_words,
                            datum.sounds.master_sound.num_syllables,
                            datum.sounds.user_sound.language,
                            datum.sounds.user_sound.phrase1,
                            datum.sounds.user_sound.phrase2,
                            datum.sounds.user_sound.speaker_name,
                            datum.sounds.user_sound.sex,
                            datum.sounds.user_sound.voice,
                            datum.sounds.user_sound.num_words,
                            datum.sounds.user_sound.num_syllables,
                            datum.u_len,
                            result.added_silence,
                            datum.compare_time,
                            result.server,
                            result.server_version,
                            result.date,
                            result.associated_test.name,
                            result.jsession_id
                    ]

                    for key in max_dict.keys():
                        segments = datum.segments.filter(type_of_speech=key) # All segments of type 'key' (i.e. - WORD)
                        for segment in reversed(segments):
                            row.append(segment.raw_status) # Add status to row
                            row.append(segment.score) # Add score to row
                            row.append(segment.transcription) # Add transcription to row

                        # If there are fewer segments than the max-number, make sure these
                        # cells are empty
                        for ii in range(max_dict[key] - len(segments)):
                            row.append('')
                            row.append('')
                            row.append('')
                    data_writer.writerow(row)

            csv_buffer.seek(0)
        
        if not request.POST.get("runs[]"):
            return HttpResponse("YOU NEED TO SELECT AT LEAST ONE RUN. NOW USE THE BACK BUTTON >:(")

        if request.POST.get("download_csv"):
            filename = '{}-{}.csv'.format(results[0].associated_test.slug, timezone.now().strftime("%Y-%m-%d_%H-%M-%S"))
            response = HttpResponse(csv_buffer, content_type='application/x-download')
            response['Content-Disposition'] = 'attachment;filename={}'.format(filename)
            return response

        elif request.POST.get("explore_data"):
            tmp_csv_file = tempfile.NamedTemporaryFile(mode='w', dir='./tmp_data', delete=False, encoding='utf-8')
            shutil.copyfileobj(csv_buffer, tmp_csv_file)
            tmp_csv_file.seek(0)
            shutil.chown(tmp_csv_file.name, user=999, group=999)
            shiny_url = '/soundshine?tmp_file_name=' + tmp_csv_file.name.replace('/soundstat/tmp_data/','')
            return redirect(shiny_url)

from django.views.generic import ListView
from soundtests.models import TestConfig
from soundresults.models import UserPhraseDatum

class ViewResults(ListView):
    """Lists all data for a particular run"""
    context_object_name = 'data'
    model = UserPhraseDatum
    paginate_by = 100
    pk_url_kwarg = 'config_pk'
    template_name = 'soundresults/view_results.html'

    def get_queryset(self):
        associated_config = TestConfig.objects.get(pk=self.kwargs.get('config_pk'))
        return UserPhraseDatum.objects.filter(result_from=associated_config)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        config = TestConfig.objects.get(pk=self.kwargs.get('config_pk'))
        context['config'] = config 
        context['test_slug'] = config.associated_test.slug

        return context

from django.views.generic import ListView
from soundtests.models import TestConfig
from soundresults.models import SegmentDatum, UserPhraseDatum

class ViewSegments(ListView):
    """Lists all segments for a particular datum"""
    context_object_name = 'segments'
    model = SegmentDatum
    template_name = 'soundresults/view_segments.html'

    def get_queryset(self):
        datum = UserPhraseDatum.objects.get(pk=self.kwargs.get('datum_pk'))
        return SegmentDatum.objects.filter(associated_datum=datum, parent_segment__isnull=True)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        datum = UserPhraseDatum.objects.get(pk=self.kwargs.get('datum_pk'))
        config = datum.result_from
        context['datum'] = datum
        context['config'] = config

        return context

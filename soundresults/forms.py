from django import forms
from django.utils import timezone
from soundtests.models.testmodels import SoundTest, TestConfig

class TestFilterForm(forms.Form):
    """
    This form filters possible tests that can be run
    """
    test_name = forms.MultipleChoiceField(
            widget=forms.SelectMultiple(
                attrs={
                    "onChange":"filter_tests(this)",
                    'size':10
                }
            ),
            required=False
    )

    server_version = forms.MultipleChoiceField(
            widget=forms.SelectMultiple(
                attrs={'onChange':'filter_tests(this)'}
            ),
            required=False
    )
    date_start = forms.DateField(
            widget=forms.TextInput(
                attrs={
                    'class':'datepicker',
                    'onSelect':'filter_tests(this)'
                }
            ),
            required=False
    )
    date_end = forms.DateField(
            widget=forms.TextInput(
                attrs={
                    'class':'datepicker',
                    'onSelect':'filter_tests(this)'
                }
            ),
            required=False
    )
    jsession_id = forms.MultipleChoiceField(
            widget=forms.SelectMultiple(
                attrs = {
                    'size':10
                }
            )
    )

    def __init__(self, *args, **kwargs):
        super(TestFilterForm, self).__init__(*args, **kwargs)
        self.fields['test_name'].choices = \
                [[d['name'], d['name']] for d in SoundTest.objects.order_by('name').values('name').distinct()]
        self.fields['server_version'].choices = \
                [[d['server_version'], d['server_version']] for d in TestConfig.objects.order_by('server_version').values('server_version').distinct()]

import boto3
import os

class S3Connection:
    """Represents connection to TLO's S3 servers
    
    AWS_ACCESS_KEY and AWS_SECRET_KEY should be set either through the Gunicorn
    Docker container's entrypoint (in production) or via the script run in the
    virtual environment (in development)
    
    """

    def __init__(
            self, 
            access_key=os.environ.get('AWS_ACCESS_KEY'), 
            secret_key=os.environ.get('AWS_SECRET_KEY'),
            bucket_name=os.environ.get('AWS_BUCKET_NAME')
    ):
        if access_key is None or secret_key is None or bucket_name is None:
            raise TypeError(
                ("No arguments for access_key, secret_key, or bucket_name "
                 "were passed in, and at least one of the environment "
                 "variables AWS_ACCESS_KEY, AWS_SECRET_KEY, or "
                 "AWS_BUCKET_NAME are not set.")
            )
        self.session = boto3.Session(
                aws_access_key_id=access_key,
                aws_secret_access_key=secret_key
        )
        self.resource = self.session.resource('s3')
        self.bucket = self.resource.Bucket(name=bucket_name)

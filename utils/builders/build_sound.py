import pydub
import requests
import tempfile

def build_sound(url, config):
    """Deals with processing sounds, including downloading, adding silence, etc.

    Any additional processing of the sounds to be sent to the server should be
    included in this function. (e.g. - In the future we may want to standardize
    a means of degrading sound or adding noise. That should be done via this 
    function). 
    
    An initial sound file is written and turned into an pydub.AudioSegment. As of now,
    this file is what is worked on. The alteration is then exported to a final
    sound file, which is the file object that is returned and sent to the
    server.

    Args:
        url (str): URL from which sound is downloaded; this URL must point to
        a WAV file or an error will be raised.

        config (:class:`TestConfig`): Test config. instance of test that is
        being run. This is where one should define the parameters for how the
        downloaded sound is to be altered.

    """

    sound = requests.get(url, verify=False).content
    final_sound_file = tempfile.NamedTemporaryFile()


    # Create PyDub pydub.AudioSegment
    try:
        initial_sound_file = tempfile.NamedTemporaryFile()
        initial_sound_file.write(sound)
        initial_sound_file.seek(0) # Go back to beginning of file
        audio = pydub.AudioSegment.from_wav(initial_sound_file)

    # The initial_sound_file lines here seem redundant, but are necessary;
    # if the below exception is thrown, then the original initial_sound_file
    # (from the 'try' clause above) is closed/deleted, and must be re-created
    except pydub.exceptions.CouldntDecodeError:
        initial_sound_file = tempfile.NamedTemporaryFile()
        initial_sound_file.write(sound)
        initial_sound_file.seek(0) # Go back to beginning of file
        audio = pydub.AudioSegment.from_mp3(initial_sound_file)

    # Adding silence if config. calls for it
    if config.added_silence:
        audio = pydub.AudioSegment.silent(duration=config.silence_before) + audio + \
                pydub.AudioSegment.silent(duration=config.silence_after)

    audio.export(final_sound_file, format='wav')
    final_sound_file.seek(0)

    return final_sound_file

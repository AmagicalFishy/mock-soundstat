from soundresults.models import UserPhraseDatum

def build_datum(result_from, pair, **kwargs):
    """Deals with putting together a point of data (i.e. - CSV row)

    Any processing of the data should be included in this function (e.g. - In 
    the future, we may want to add specific error checking, or fill a CSV row
    with a default, CSV-appropriate "NULL" entry if the server-returned data
    is not sufficient; this ensures that generated CSVs don't throw errors when
    passed to the graphing module and allows us to define logic more specific 
    than the defaults provided in the model).
    
    Args:
        result_from (:class:`TestConfig`): Test config. instance of the test
        that is being run, from which this data comes. 
        
        pair (:class:`TestPair`): Specific test pair instance, which
        indicates a user sound and a master sound to which it is sent.

        **kwargs (dict): A dictionary which contains all the additional info.
        Each key in the dictionary should correspond to a field in the 
        :class:`UserPhraseDatum` model. We use a generic argument dictionary 
        here instead of including an argument for every field because these 
        fields change relatively often. Also, because we have the option of 
        providing defaults with the dictionary's get() method.
    
    """

    datum = UserPhraseDatum(
            result_from=result_from,
            sounds=pair,
            score=kwargs.get('score', None),
            snr=kwargs.get('snr', None),
            u_len=kwargs.get('u_len', None),
            vad_start=kwargs.get('vad_start', None),
            vad_end=kwargs.get('vad_end', None),
            jsession_id=kwargs.get('jsession_id', None)
    )
    datum.save()

    return datum

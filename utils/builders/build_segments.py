from soundresults.models import SegmentDatum

def build_segments(segment_json, datum, _parent=None):
    """Deals with putting together the individual segments of a spoken phrase

    Any processing of the segments should be included in this function
    (e.g. - In the future we may want to map the returned raw_status
    something more readable).
    
    Args:
        segment_json (list): A dictionary containing the segment json returned 
        from the server after comparing a sound. As of 2019.03.01, its key is
        'segments'. This should return a list of dictionaries.

        datum (:class:`UserPhraseDatum`): The specific datum to which these
        segments should be associated

        _parent (:class:`PartOfSpeechDatum`): The _parent of which a segment is a
        child. For example, a SYLLABLE will be the child of some WORD, and a
        PHONEME will be the child of some SYLLABLE. This should only be used
        internally
    
    """

    for segment in segment_json:
        try:
            # Create part of speech for each highest-level segment in provided json
            segment_datum = SegmentDatum(
                    associated_datum = datum,
                    type_of_speech = segment.get('type', 'NONE'),
                    score = segment.get('score', -2),
                    master_start = segment.get('masterStart', -1),
                    master_end = segment.get('masterEnd', -1),
                    user_start = segment.get('userStart', -1),
                    user_end = segment.get('userEnd', -1),
                    text = segment.get('text', 'NONE'),
                    transcription = segment.get('transcription', 'NONE'),
                    status = segment.get('status', 'NONE'),
                    raw_status = segment.get('rawStatus', 'NONE'),
            )

            # Set _parent if _parent was provided
            if _parent:
                segment_datum.parent_segment = _parent

            segment_datum.save()

            if segment['segments'] is not None: # If this segment has its own segments...
                build_segments(segment['segments'], datum, _parent=segment_datum) 

        except TypeError:
            pass

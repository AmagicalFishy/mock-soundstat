import random
import string


def random_gen(size=16, chars=string.ascii_uppercase + string.digits):
    """Generates random alphanumeric string"""
    return ''.join(random.choice(chars) for ii in range(size))

from django.http import HttpResponse
from django.shortcuts import redirect, render, reverse

from soundtests.models import SoundTest

def select_test(request):
    """View where user selects tests they'd like to run
    
    The respective template is just an HTML form + table with the test 
    titles and checkboxes. Users check the tests they'd like to run and 
    hit the submit button

    """

    if request.method == 'GET':
        test_info = []
        tests = SoundTest.objects.all()
        for test in tests: # Append test info. to list, send to template
            test_info.append([test.name, test.description, test.last_run])

        return render(request, 'soundcomm/select_test.html', {'test_info': test_info }) 

    # Send user back if user came here through some weird method that wasn't
    # just clicking on the link or something (why would anyone come to this page
    # with a POST?)

    else:
        return redirect(reverse("soundtests:index"))

        

from celery import chain, chord
from django.forms import formset_factory
from django.shortcuts import render, redirect
from soundcomm.forms import ConfigBaseFormSet, ConfigForm
from soundcomm.tasks import end_test, init_test, run_master_sound_set


def config_test(request):
    """The view where the user configures the tests they'd like to run

    Users can set SNR, whether or not silence is added to the beginnings/ends
    of every sound, which server to make the comparisons on (Live/Test), etc.
    When each test is appropriately configured, for each test, a chain of
    tasks is created and sent to Celery:

    - :ref:`soundcomm.tasks.init_test`
    - :ref:`soundcomm.tasks.run_lesson`
    - :ref:`soundcomm.tasks.end_test`

    """

    ConfigFormSet = formset_factory(form=ConfigForm, formset=ConfigBaseFormSet, extra=0)
    if request.method == 'GET':
        initial_info = [] # List of dictionaries for initial argument
        for test in request.GET.getlist('selected_tests'):
            initial_info.append({'associated_test': test})
        formset = ConfigFormSet(initial=initial_info)
        return render(request, 'soundcomm/config_test.html', {'formset': formset})

    if request.method == 'POST':
        formset = ConfigFormSet(request.POST)
        if formset.is_valid():
            for form in formset:
                # The save() method of this form returns the form's instance
                # of TestConfig
                config = form.save()
                # In a Celery chain, w/e the task before it returns is
                # automatically passed as an argument to the next task
                workflow = chain(
                        init_test.s(),
                        chord(
                            [run_master_sound_set.s(sound_pk=sound.pk) for sound in config.associated_test.master_sounds],
                            end_test.s()
                        )
                )
                workflow.delay({'config_pk': config.pk})

            return redirect('soundcomm:loading_page')
        return render(request, 'soundcomm/config_test.html', {'formset': formset})

    else:
        return redirect('soundcomm:config_test')

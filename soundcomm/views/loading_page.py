from django.shortcuts import render

def loading_page(request):
    return render(request, 'soundcomm/loading_page.html')

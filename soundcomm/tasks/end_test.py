from celery.decorators import task
from django.utils import timezone
from soundtests.models import TestConfig

@task(name='soundcomm.EndTest', max_retries=10)
def end_test(config_pk):
    """Closes the test out

    This saves the particular test run's configuration and populates the last
    run time of the test instance.

    Any logic that happens after all of the pairs in a test are sent to the 
    server and scored should go here.

    Args:
        config_pk (list): A list of the primary keys `:class:TestConfig`, the
        particular config. instance that represents a specific running of a 
        test. In usage, this argument is passed implicitly via the result-list
        of the preceeding tasks. (Every entry in the list is the same; it is 
        a list by virtue of Celery's chord signature. See 
        :ref:`soundcomm.views.config_test`).

    """

    config = TestConfig.objects.get(pk=config_pk[0])
    test = config.associated_test

    test.last_run = timezone.now()
    test.save()
    config.date = test.last_run
    config.success = True
    config.save()

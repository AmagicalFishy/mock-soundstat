from .server_comm import ServerComm
from celery.decorators import task
from django.db.utils import OperationalError
from django.utils import timezone
from soundtests.models import TestConfig

@task(
        name='soundcomm.InitTest', 
        auto_retry_for=(OperationalError,),
        retry_kwargs={'max_retries': 10}
)
def init_test(kwargs):
    """Contacts the SA Sound Server, assigns a server version and jsessionid

    Despite (often) having multiple lessons, each test is assigned only one
    jsessionid. This just makes it easier to keep tests as a group (when 
    writing reports, for example, it's much easier to link one test with one
    jsession id than it is one test to many ids.)
   
    This task returns a dictionary which is passed to the :ref:`run_lesson`
    task.

    """
    config = TestConfig.objects.get(pk=kwargs.pop('config_pk'))

    print('Making contact w/ server...')
    server_comm = ServerComm(config.server)
    config.server_version = server_comm.version
    config.jsession_id = server_comm.jsessionid
    print('JSessionID: {}'.format(server_comm.jsessionid))
    config.date = timezone.now()
    config.save()
    print('Saved TestConfig instance...')

    returned_data = {
            'server_comm': server_comm.__dict__,
            'config_pk': config.pk,
    }
    return returned_data

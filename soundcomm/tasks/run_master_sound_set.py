from .server_comm import ServerComm
from celery.decorators import task
from django.utils import timezone
from soundtests.models import Lesson, Sound, TestConfig
from soundresults.models import UserPhraseDatum
from utils.builders import build_datum, build_sound, build_segments

import json
import requests
import time
import urllib3
import wave

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

@task(name='soundcomm.RunMasterSoundSet', max_retries=10, time_limit=1800)
def run_master_sound_set(init_data, sound_pk):
    master_sound = Sound.objects.get(pk=sound_pk)
    lesson = master_sound.associated_lesson
    config = TestConfig.objects.get(pk=init_data.pop('config_pk'))
    test = config.associated_test
    server_comm = ServerComm(serialized_instance=init_data.pop('server_comm'))

    # Send server appropriate lesson and jsessionid
    server_comm.set_lesson(lesson=lesson)

    # This repetition adds a bit of overhead, but we do it in case of an error;
    # This way, if this tasks fails for some reason, the config will be 
    # populated with default-valued data (this allows for viewing of
    # partially completed tests)
    
#    print('Generating empty data for master sound: {}'.format(master_sound))
    test_pairs = test.pairs_to_test.filter(master_sound=master_sound)
    for pair in test_pairs:
        if UserPhraseDatum.objects.filter(result_from=config, sounds=pair).exists():
            continue
        else:
            datum = build_datum(result_from=config, pair=pair, **{'jsession_id': server_comm.jsessionid})

    for pair in test_pairs:
        print('Testing: {} --> {}'.format(pair.user_sound.phrase2, pair.master_sound.phrase2))
        # Process sound (add silence., etc.)
        user_sound = build_sound(pair.user_sound.url, config)
            
        # Compare user and master sounds (try 5 times if result is -1)
        tries = 0 
        while tries < 5:
            byte_response = server_comm.compare_sounds(user_sound=user_sound, master_sound=pair.master_sound, method=config.scoring).text
            response = json.loads(byte_response)
            if response['compareResult'][0] == -1:
                tries += 1
            else:
                tries = 5

        ### If score is below what it should be (i.e. -1) try mulitple times  *** NOT IMPLEMENTED ###

        # Fill out and save data row
        print('Score: {}'.format(response['compareResult'][0]))
        print('-----')
        opened_user_sound = wave.open(user_sound.name, 'rb')
        user_sound_length = opened_user_sound.getnframes()/float(opened_user_sound.getframerate())

        data_dict = {
                'score': response['compareResult'][0],
                'snr': 25,
                'u_len': user_sound_length,
                'compare_time': server_comm.last_response_time,
                'jsession_id': server_comm.jsessionid
        }

        # If score is -1, these will not have entries
        try:
            data_dict['vad_start'] = response['userDetails']['vad']['start']
            data_dict['vad_end'] = response['userDetails']['vad']['end']
        except TypeError:
            pass

        datum = UserPhraseDatum.objects.filter(result_from=config, sounds=pair) # There should only be one
        datum.update(**data_dict)
        datum = datum[0] # Turns the Queryset into a single instance
        if response['segments'] is not None:
            build_segments(segment_json=response['segments'], datum=datum)

    return config.pk # Will be passed onto the "end_test" task

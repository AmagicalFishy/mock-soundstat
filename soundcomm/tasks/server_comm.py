from base64 import b64encode

import json
import requests
import time

class ServerComm:
    """Deals specifically with communicating with the server"""

    _serverlist_url = '{server}/v2/sound/serverlist;jsessionid={jsessionid}'
    _compare_url = '{server}/v2/sound/compare;jsessionid={jsessionid}'

    def __init__(self, server=None, serialized_instance=None):
        """ Set instance specific parameters 

        When an instance of this class is created, it requests the server 
        version, gets a jsession id, and sets the URLs and header according 
        to this info appropriately.

        Args:
            server (string): One of the appropriate server choices. The choices
            are found in the SERVER_CHOICES tuple in :class:`TestConfig`
        
        """
        self.mode = "" # Scoring Mode
        self.appName = "" # App. Name (for scoring mode)
        if server:
            # Session parameters
            self.version = requests.get('{}/sound/version/plain'.format(server)).text
            self.jsessionid = requests.get('{}/v2/sound/session/true'.format(server)).text
            self.serverlist_url = self._serverlist_url.format(server=server, jsessionid=self.jsessionid)
            self.compare_url = self._compare_url.format(server=server, jsessionid=self.jsessionid)

            # Headers
            self.headers = {
                    'X-JSESSIONID': self.jsessionid,
                    'Content-Type': 'application/json'
            }

        elif serialized_instance: 
            self.__dict__.update(**serialized_instance)

        else:
            raise TypeError('This class must be initialized with either \
            the "server" argument or the "serialized_instance" argument')
        
    def set_lesson(self, lesson):
        """Creates lesson JSON and sends lesson to server

        Args:
            lesson (:class:`Lesson`): The lesson instance which contains the 
            master sounds to be tested
        
        """
        self.lesson_json = lesson.json
        post = requests.post(
                url=self.serverlist_url,
                headers=self.headers,
                data=json.dumps(self.lesson_json)
        )
        return post

    def compare_sounds(self, user_sound, master_sound, method="pro"):
        """Sends the user sound to the server to be compared w/ the master sound

        Args:
            user_sound (file): A file object with the user sound. In normal usage,
            this is the file returned by :func:`builders.build_sound()`.

            master_sound (:class:`Sound`): The Sound instance of the master 
            sound. This sound needs to come from uploading the lesson JSON 
            (specifically, the lesson set in :func:`set_lesson()`.
       
        Returns:
            requests.models.Response: A response object representing the 
            server's response. This contains the score, segments, etc. If the
            returned response is 'x', it can be turned into a usable 
            dictionary with json.loads(x.text)

        """
        # Set scoring method
        if method=="pro":
            self.mode = "Pronunciation"
            self.appName = "ac-pronunciation"
        elif method=="spv":
            self.mode = "Selfreporting"
            self.appName = "ac-selfreporting"

        print(self.mode)
        print(self.appName)
        data = {
                "ticket": "QA SA Testing",
                "soundData": b64encode(user_sound.read()).decode('ascii'),
                "sounds": [{
                    "url": master_sound.lesson_url,
                    "priority": 0,
                    "soundData": "",
                    "hash": master_sound.lesson_hash,
                    "phrase1": master_sound.phrase1,
                    "phrase2": master_sound.phrase2,
                    "phraseTranslit": "",
                    "learningLanguageCode": self.lesson_json['languageLearn'],
                    "knownLanguageCode": self.lesson_json['languageNative'],
                    "precompareScore": 0
                }],
                "user_sound_snr": 25,
                "correctSoundHash": master_sound.lesson_hash,
                "phrase1": master_sound.phrase1,
                "phrase2": master_sound.phrase2,
                "listName": self.lesson_json['listName'],
                "activityType": self.lesson_json['activityType'],
                "languageLearn": self.lesson_json['languageLearn'],
                "languageNative": self.lesson_json['languageNative'],
                "mode": self.mode,
                "appName": self.appName,
                "version": "1.0.0",
                "device": "",
                "gain": 0,
                "vad_start": 0,
                "type": "wav"
        }

        start_time = time.time()
        response = requests.post(
                url=self.compare_url,
                headers=self.headers,
                data=json.dumps(data)
        )
        end_time = time.time()
        self.last_response_time = round(end_time - start_time, 2)

        return response

from django import forms
from django.forms import formsets

def find_common_hash(sound_hash_list):
    """Determines if any two sets of hashes intersect

    This recursively goes through the provided TestConfigs and ensures that
    none of the tests-to-be-run share the same sound (this is because, unless
    tests are run four hours apart, the sound is cached on the server and will
    not be re-analyzed appropriately).

    Args:
        sound_hash_list: A list of lists with the format 
        ['test_name', ['hash-1', 'hash-2', 'hash-3', ...]], where each 'hash-#' 
        is an alphanumeric strings representing the SHA1 hash of user 
        :class:`Sound`s associated with the test.
    """

    if len(sound_hash_list) == 1:
        return False

    cur_test = sound_hash_list.pop()
    for comp_test in sound_hash_list:
        if not set(cur_test[1]).isdisjoint(comp_test[1]): # ... if the two groups share an element
            return [cur_test, comp_test]

    return find_common_hash(sound_hash_list)


class ConfigBaseFormSet(formsets.BaseFormSet):
    """Formset for ConfigForm
    
    The only difference is the clean method, used to ensure duplicate sounds aren't
    used in the same set of test runs. 
    """

    def clean(self):
        """Overriden clean method using :meth:`find_common_hash`

        This overridden clean method determines whether or not any two tests have the 
        same user sound and server; if they do, these two tests cannot be run at the 
        same time due to the way the server caches sounds
        """

        cleaned_data = super(ConfigBaseFormSet, self).clean()
        sound_hashes = {}
        for form in self.forms:
            if not form.cleaned_data['added_silence']:
                try:
                    server = form.cleaned_data['server']
                except KeyError: # In case one of the forms has an incorrect server
                    continue
                associated_test = form.cleaned_data['associated_test'] # Get its associated test
                if server not in list(sound_hashes.keys()):
                    sound_hashes[server] = [] # Add key according to server if not already there
                sound_hashes[server].append([associated_test.name, []]) 
                for pair in associated_test.pairs_to_test.all():
                    sound_hashes[server][-1][1].append(pair.user_sound.sound_hash)

        for key in list(sound_hashes.keys()):
            conflicting_tests = find_common_hash(sound_hashes[key])
            if conflicting_tests:
                raise forms.ValidationError(
                        "The tests " + conflicting_tests[0][0] + " and " +
                        conflicting_tests[1][0] + " cannot be run at the same time on the same " +
                        "server. They each contain an identical user sound. Due to the way the " +
                        "way the server caches sounds, these tests must be run at least 4 hours " +
                        "apart or at least one of them must be run with 'Added Silence' checked. ", 
                        "same_sound"
                )

        return cleaned_data

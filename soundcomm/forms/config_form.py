from django import forms
from django.utils import timezone
from soundtests.models import SoundTest, TestConfig

class ConfigForm(forms.ModelForm):
    """Form used when user configures a particular test run """

    class Meta:
        model = TestConfig
        fields = [
                "associated_test", "gain", "vad_start", "user_sound_snr", "server",
                "scoring", "added_silence", "silence_before", "silence_after", "random_snr"
        ]

    def __init__(self, *args, **kwargs):
        super(ConfigForm, self).__init__(*args, **kwargs)
        self.fields['associated_test'] = forms.CharField()
        self.fields['associated_test'].widget.attrs['readonly'] = True

    def clean(self):
        """Overriden clean method to ensure sounds aren't duplicated"""
        cleaned_data = super(ConfigForm, self).clean()
        cleaned_data['associated_test'] = SoundTest.objects.get(name=self.cleaned_data['associated_test'])
        if not cleaned_data['added_silence']:
            four_hour_hashes = []
            these_hashes = []

            # For all tests that have been run in the past 4 hours, get the user-sound hashes (if the test
            # was run on the same server)
            try:
                for config in TestConfig.objects.filter(
                        date__gt = timezone.now() - timezone.timedelta(hours=4),
                        added_silence = False,
                        server = cleaned_data['server']
                ):
                    for pair in config.associated_test.pairs_to_test.all():
                        four_hour_hashes.append(pair.user_sound.sound_hash)
            
            except KeyError: # In case user (somehow) inputs an invalid choice
                raise forms.ValidationError("Invalid server choice")

            # Get the sound hashes for this test
            for pair in self.cleaned_data['associated_test'].pairs_to_test.all():
                these_hashes.append(pair.user_sound.sound_hash)

            if len(these_hashes) > len(set(these_hashes)):
                raise forms.ValidationError(
                        "This test contains multiple instances of the same user sound. Due to " + 
                        "the way the server caches sounds, you must run this test with 'Added " +
                        "Silence' checked.", "multiple_sound_same_test"
                )

            if not set(these_hashes).isdisjoint(four_hour_hashes):
                raise forms.ValidationError(
                    "You must run this test with 'Added Silence' checked. " +
                    "This test contains a user sound identical to one that has been sent " +
                    "to the server within the past four hours.", 
                    "4_hour_conflict"
                )

        return cleaned_data


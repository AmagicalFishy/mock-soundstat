from django.apps import AppConfig


class SoundcommConfig(AppConfig):
    name = 'soundcomm'

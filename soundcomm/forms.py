# -*- coding: utf-8 -*-
import logging

from django import forms
from django.forms import formsets, ModelForm, Form
from django.utils import timezone
from soundtests.models.testmodels import SoundTest, TestConfig
from soundtests.models.sound import Sound

logger = logging.getLogger('standard_logger')

def find_common_hash(sound_hash_list):
    """ 
    This is just a helper function which determines if any two 
    sets of sound hashes intersect at all. It is used in the
    clean method of the ConfigFormSetFactory.
    """

    if len(sound_hash_list) == 1:
        return False

    cur_test = sound_hash_list.pop()
    for comp_test in sound_hash_list:
        if not set(cur_test[1]).isdisjoint(comp_test[1]): # ... if the two groups share an element
            return [cur_test, comp_test]

    return find_common_hash(sound_hash_list)

    
class ConfigForm(ModelForm):
    """
    Form used when user configures a particular test run
    """

    class Meta:
        model = TestConfig
        fields = [
                "associated_test", "gain", "vad_start", "user_sound_snr", "server", 
                "added_silence", "silence_before", "silence_after", "random_snr"
        ]

    def __init__(self, *args, **kwargs):
        logger.info("Initializing ConfigForm...")
        super(ConfigForm, self).__init__(*args, **kwargs)
        self.fields['associated_test'] = forms.CharField()
        self.fields['associated_test'].label = "Test"
        self.fields['associated_test'].widget.attrs['readonly'] = True
        self.fields['user_sound_snr'].label = "User Sound SNR"

    def clean(self):
        logger.info("Verifying config. form...")
        cleaned_data = super(ConfigForm, self).clean()
        cleaned_data['associated_test'] = SoundTest.objects.get(name=self.cleaned_data['associated_test'])
        logger.info(self.cleaned_data['associated_test'])
        if not cleaned_data['added_silence']:
            four_hour_hashes = []
            these_hashes = []

            # For all tests that have been run in the past 4 hours, get the user-sound hashes (if the test
            # was run on the same server)
            for config in TestConfig.objects.filter(
                    date__gt = timezone.now() - timezone.timedelta(hours=4), 
                    added_silence = False,
                    server = cleaned_data['server']):
                for pair in config.associated_test.pairs_to_test.all():
                    four_hour_hashes.append(pair.user_sound.sound_hash)

            # Get the sound hashes for this test
            for pair in self.cleaned_data['associated_test'].pairs_to_test.all():
                these_hashes.append(pair.user_sound.sound_hash)

            if len(these_hashes) > len(set(these_hashes)):
                raise forms.ValidationError(
                        "This test contains multiple instances of the same user sound. Due to " + 
                        "the way the server caches sounds, you must run this test with 'Added " +
                        "Silence' checked.", "multiple_sound_same_test"
                )
                
            if not set(these_hashes).isdisjoint(four_hour_hashes):
                raise forms.ValidationError(
                    "You must run this test with 'Added Silence' checked. " +
                    "This test contains a user sound identical to one that has been sent " +
                    "to the server within the past four hours.", 
                    "4_hour_conflict"
                )

        return cleaned_data

class ConfigBaseFormSet(formsets.BaseFormSet):
    """
    Formset for ConfigForm; the only difference is the clean method, used to ensure duplicate
    sounds aren't used in the same test runs. 
    """

    def clean(self):
        """
        This overridden clean method determines whether or not any two tests have the 
        same user sound and server; if they do, these two tests cannot be run at the 
        same time due to the way the server caches sounds
        """
        logger.info("Ensuring tests don't contain same sound...") 
        cleaned_data = super(ConfigBaseFormSet, self).clean()
        sound_hashes = {}
        for form in self.forms:
            if not form.cleaned_data['added_silence']:
                server = form.cleaned_data['server']
                associated_test = form.cleaned_data['associated_test'] # Get its associated test
                if server not in list(sound_hashes.keys()):
                    sound_hashes[server] = [] # Add key according to server if not already there
                sound_hashes[server].append([associated_test.name, []]) 
                for pair in associated_test.pairs_to_test.all():
                    sound_hashes[server][-1][1].append(pair.user_sound.sound_hash)

        for key in list(sound_hashes.keys()):
            conflicting_tests = find_common_hash(sound_hashes[key])
            if conflicting_tests:
                raise forms.ValidationError(
                        "The tests " + conflicting_tests[0][0] + " and " +
                        conflicting_tests[1][0] + " cannot be run at the same time on the same " +
                        "server. They each contain an identical user sound. Due to the way the " +
                        "way the server caches sounds, these tests must be run at least 4 hours " +
                        "apart or at least one of them must be run with 'Added Silence' checked. ", 
                        "same_sound"
                )

        return cleaned_data

from django.urls import path
from . import views

app_name = 'soundcomm'

urlpatterns = [
    path('select_test', views.select_test, name='select_test'),
    path('config_test', views.config_test, name='config_test'),
    path('loading_page', views.loading_page, name='loading_page')
]

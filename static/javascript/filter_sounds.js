// Used with "soundstat/soundtests/forms/_filterform.py" and all classes
// which inherit from it. This updates the choices of sound when creating
// tests, associating sounds, etc.

function filter_sounds(form_field, lessons=false, recording=false){
    /* Grabs the different attributes selected in the form
    The "lessons" parameter determines when sounds are
    filtered by whether or not they're part of a lesson (and can
    thus be used as master sounds). The "recording" parameter 
    determines whether or not to return only unique instances 
    of a phrase (i.e. - If there are 20 recordings of the word 
    "dog", and recording is set to true, then only one "dog" option 
    will be shown. */

    var id_num = form_field.id.replace(/[^0-9]/gi, '');
    var sex_selection = $("#id_form-" + id_num + "-sex").val();
    var speaker_selection = $("#id_form-" + id_num + "-speaker_name").val();
    var language_selection = $("#id_form-" + id_num + "-language").val();
    var num_syllable_selection = $("#id_form-" + id_num + "-num_syllables").val();
    var num_words_selection = $("#id_form-" + id_num + "-num_words").val();
    var voice_selection = $("#id_form-" + id_num + "-voice").val();
    var unique_phrase_selection = $("#id_form-" + id_num + "-unique_phrase").val();
  
    /* Sends an AJAX request. Basically what this does is tell the server which
    attributes (filters) are selected, and the server spits out all phrases which
    match these filters. Then, on success, this function repopulates
    the options-select field */

    $.ajax({
        'method': 'GET',
        'url': '/soundtests/sound_filter',
        'data': {
            'sex': sex_selection, 
            'language': language_selection,
            'speaker': speaker_selection,
            'num_syllables': num_syllable_selection,
            'num_words': num_words_selection,
            'voice': voice_selection,
            'unique_phrase': unique_phrase_selection,
            'lessons': lessons,
            'recording': recording,
            'csrfmiddlewaretoken': $("[name=csrfmiddlewaretoken]").val()
        },
        success: function(dataReturned) {
            if (!recording) { 
                var unique_phrases_form = $("#id_form-" + id_num + "-unique_phrase");
                unique_phrases_form
                    .find('option')
                    .remove()
                    .end()
                ;
                new_options = JSON.parse(dataReturned) 
                for (ii = 0; ii < new_options.length; ii++) {
                    var phrase1 = new_options[ii]['phrase1'];
                    var phrase2 = new_options[ii]['phrase2'];
                    unique_phrases_form
                        .append('<option value="' + phrase1 + '">' + phrase2 + ' : ' + phrase1 + '</option>')
                   ;
                }
            } else {
                var displayed_sounds = $("#id_form-" + id_num + "-sounds_to_send");
                displayed_sounds
                    .find('option')
                    .remove()
                    .end()
                ;
                new_options = JSON.parse(dataReturned) 
                for (ii = 0; ii < new_options.length; ii++) {
                    var sound_info = new_options[ii];
                    displayed_sounds
                        .append(
                                `<option 
                                 value="${sound_info['key']}" 
                                 pk="${sound_info['key']}" 
                                 id="option-${id_num}-${ii}" 
                                 ondblclick=play_sound(this) 
                                 language="${sound_info['language']}" 
                                 phrase1="${sound_info['phrase1']}" 
                                 phrase2="${sound_info['phrase2']}" 
                                 > 
                                 ${sound_info['display']} ${sound_info['phrase2']} : ${sound_info['phrase1']}
                                 </option>`
                        )
                        .val(sound_info['key'])
                   ;
                }

            }
        }
    });
}


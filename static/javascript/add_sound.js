/* Used during test creation. This is called when a user clicks
 * the "Add Selected Sounds to Test" button. It does two things:
 * First, it creates a hidden input field in which the selected
 * sound's primary key will be stored. Second, it adds a row
 * to the table which lists the sounds that were added so far
 */
function add_sound(){
    $('#id_form-0-sounds_to_send :selected').each(function(selected) {
        $('#master_sounds').append(
                '<input type="hidden" name="master_sounds[]" value=' + $(this).attr('pk') + 
                ' class=' + $(this).attr('pk') + '-hidden>');
        $('#test_table > tbody:last-child').append(
                '<tr class=' + $(this).attr('pk') + '>' +
                    '<td>' + $(this).attr('language') + '</td>' +
                    '<td>' + $(this).attr('phrase1') + '</td>' +
                    '<td>' + $(this).attr('phrase2') + '</td>' +
                    '<td><a href="javascript:void(0);"' +
                    ' onclick=remove_sound("' + $(this).attr('pk') + '")>X</a></td>' +
                '</tr>'
        );
        $('.' + $(this).attr('pk')).not(':first').remove();
        $('.' + $(this).attr('pk') + '-hidden').not(':first').remove();
    })
}

/* This removes the sound from the test-creation table
 * upon a user clicking the "X" 
 */
function remove_sound(pk){
        $('.' + pk).remove();
        $('.' + pk + '-hidden').remove();
}



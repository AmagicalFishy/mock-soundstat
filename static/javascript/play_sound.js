// This function just plays the sound at a given URL
// If no URL exists, an AJAX request fetches one. The
// given element must have a "pk" attribute which is 
// the PK of the sound-to-be-played
//
// (e.g. - <a href=# pk=1234567890 onclick=play_sound(this)>)

function play_sound(selected){
    if (selected.getAttribute('url') == undefined) {
        $.ajax({
            'method': 'GET',
            'url': '/soundtests/get_url',
            'data': {
                'pk': selected.getAttribute('pk')
            },
            success: function(dataReturned) {
                selected.setAttribute('url', dataReturned['url'])
            },
            complete: function(dataReturned) {
                sound = new Audio(selected.getAttribute('url'));
                sound.play()
            }
        });
    } else {
        sound = new Audio(selected.getAttribute('url'));
        sound.play()
    }
    // The reason we need to duplicate the 'sound = new Audio ...' line is
    // because we need to wait for the AJAX call to complete. Otherwise a user
    // would have to double-click the sound *twice* (the script would try to 
    // play the sound before the AJAX call was complete, so one double-click
    // to initiate the AJAX call [and play the empty sound], and another
    // double-click to play the actual sound)
}

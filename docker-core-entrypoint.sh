#!/bin/bash
echo Waiting for database...
WAIT_COUNT=0
until nc -z ${SQL_HOST} ${SQL_PORT}; do 
    if [ $WAIT_COUNT == 300 ]; then
        echo Exiting, cannot connect to database with error: 
        nc ${SQL_HOST} ${SQL_PORT}
        exit
    fi
    WAIT_COUNT=$((WAIT_COUNT + 1))
    sleep 1;
done

mkdir -p logs/django
#touch static/logs/gunicorn.log
#touch static/logs/access.log
#touch static/logs/nginx_access.log
#touch static/logs/nginx_error.log

echo Getting AWS access key and secret key from Vault...
ROLE_ID=$ROLE_ID
SECRET_ID=$SECRET_ID
SECRET_ENDPOINT=$VAULT_ENDPOINT
VAULT_HOST="[REDACTED]"
VAULT_PORT="[REDACTED]"
VAULT_LOGIN_URL="[REDACTED]"
VAULT_SECRET_URL="[REDACTED]"
VAULT_TOKEN=$(curl -s -X POST -d '{"role_id":"'$ROLE_ID'","secret_id":"'$SECRET_ID'"}' $VAULT_LOGIN_URL | jq .auth.client_token | sed 's/"//g')
SECRET_OUTPUT=$(curl -s -H "X-Vault-Token: $VAULT_TOKEN" -H "Content-Type: application/json" $VAULT_SECRET_URL)

export AWS_ACCESS_KEY=$(echo $SECRET_OUTPUT | jq -r .data.username)
export AWS_SECRET_KEY=$(echo $SECRET_OUTPUT | jq -r .data.password)

echo "ACCESS KEY: " $AWS_ACCESS_KEY
echo "SECRET KEY: " $AWS_SECRET_KEY

echo Starting Gunicorn...
exec gunicorn soundstat.wsgi:application \
    --bind unix:${SOCKET_DIR}/gunicorn_socket.sock \
    --workers 3 \
    --timeout 300 \
    --log-level=debug \
    --log-file=static/logs/gunicorn.log \
    --access-logfile=static/logs/access.log \
    --limit-request-line 0 \
    &
echo Starting Celery worker...
exec celery -A soundstat worker -E --loglevel=info

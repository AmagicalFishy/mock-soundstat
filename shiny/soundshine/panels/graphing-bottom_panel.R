library('ggplot2')
library('R.utils')
source('graphs/choices.R')
sourceDirectory('graphs')

bottomPanelInput <- function(id) {
    ns <- NS(id)
    uiOutput(ns('bottomPanel'))
}

bottomPanel <- function(input, output, session, data, selected_graph) {
    # Call the default-value function of the graph, depending on what the selected graph is
    ns <- session$ns
    output$bottomPanel <- renderUI({
        tagList( 
            wellPanel(
                match.fun(paste(selected_graph(), '_bottom_panel', sep=''))(ns, data)
            )
        )
    })
 
    observeEvent(selected_graph(), {
        match.fun(paste(selected_graph(), '_bottom_panel_defaults', sep=''))(session, data)
    })

    # Output the parameters selected by user
    params <- reactive({ input })
    return(params)
}

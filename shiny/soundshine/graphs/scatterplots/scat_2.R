library('magrittr')
library('dplyr')
library('ggplot2')
source('graphs/common.R')

# Score vs. Duration - scat_2
scat_2 <- function(data, params) {
    sumData <- data %>%
        group_by(u_len) %>%
        summarise(mean_score = mean(score), mean_len = mean(u_len))

    graph <- ggplot(data=sumData, aes(x=mean_len, y=mean_score)) +
      geom_point(shape=1) + 
      xlab("Duration [ms]") + 
      ylab("Score") + 
      ggtitle("Duration of Sound : Score") +
      scale_x_continuous(limits = params$duration_range) +
      scale_y_continuous(limits = params$score_range)

    if (params$words) { 
        word_data <- data %>%
            group_by(unum_words) %>%
            summarise(mean_score = mean(score), mean_len = mean(u_len))
        graph = graph + 
            geom_text(data=word_data, color='salmon', fontface='bold', aes(label=unum_words))
    }
    if (params$syllables) {
        syllable_data <- data %>%
            group_by(unum_syllables) %>%
            summarise(mean_score = mean(score), mean_len = mean(u_len))
        graph = graph +
            geom_text(data=syllable_data, color='steelblue1', fontface='bold', aes(label=unum_syllables))
    }
    if (params$trend) {
        graph = graph + 
            geom_smooth(method='loess')
    }

    return(graph)
}

# Bottom Panel
scat_2_bottom_panel <- function(ns, data) {
    panel <- fluidRow(
        column(6,
            score_rangeInput(ns),
            sliderInput(
                inputId = ns('duration_range'),
                label = 'Duration of Sound [ms]',
                min = 0,
                max = 10000,
                value = c(0, 10000),
                step = 100,
                round = FALSE,
                ticks = TRUE
            )
        ),
        column(6,
            HTML('<label class="control-label">Additional Stats.</class>'),
            checkboxInput(
                inputId = ns('words'),
                label = 'Mean Score: Number of Words',
                value = FALSE
            ),
            checkboxInput(
                inputId = ns('syllables'),
                label = 'Mean Score: Number of Syllables',
                value = FALSE
            ),
            checkboxInput(
                inputId = ns('trend'),
                label = 'LOESS Regression Line',
                value = FALSE
            )
        )
    )
    return(panel)
}

# Bottom Panel Defaults
scat_2_bottom_panel_defaults <- function(session, data) { 
    updateSliderInput(session, 'score_range', value=c(min(data$score), max(data$score)))
    updateSliderInput(session, 'duration_range', value=c(min(data$u_len), max(data$u_len)))
}

# Graphing Choices for Bar Plots
graphChoices <- list(
    `Bar Graphs` = c(
#        'Mean Score vs. [Parameter]' = "bar_1",
        'Mean Smesh vs. Dandelions' = "bar_2"
    ),
    `Scatter Plots` = c(
        'Score by Phrase' = "scat_1",
        'Score vs. Duration of Sound' = "scat_2"
 #       'User Syllables Segmenting vs. Server Syllable Segmenting' = "scat_3",
 #       'User Word Segmenting vs. Server Word Segmenting' = "scat_4",
 #       'Ode to my Eyelashes' = "scat_5",
    ),
    `Other` = c(
        'Part of Speech Status Breakdown' = 'oth_1'
 #       'Trend Line showing the number of eyelashes I have' = "other_1",
 #       'Segmenting Pie Graph' = "other_2",
 #       'Problem Spotter Trend Graph' = "other_3",
    )
)

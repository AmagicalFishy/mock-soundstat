from django import forms
from hashlib import sha1
from soundtests.models import Lesson, Sound
from urllib.request import urlopen

import ssl

class LessonDataSubForm(forms.Form):
    """Defines the tiny subform used in LessonSoundFormset

    The purpose of this mini-form is to define a name, sex, and voice for 
    all sounds in a lesson at the same time (instead of doing each one individually,
    since each lesson contains only one speaker anyway).
    """
    speaker_name = forms.CharField(required=True)
    sex = forms.ChoiceField(choices=Sound.SEX_CHOICES)
    voice = forms.BooleanField(required=False)

class LessonSoundFormset(forms.BaseFormSet):
    """Formset for each sound in an uploaded lesson

    The reason nothing is saved until this formset validates is to ensure that
    any errors prevent erroneous saving. For example, if sounds were not 
    appropriately uploaded to S3, then we wouldn't want to save the sounds
    in the database. Or, if there was some error between here and when the user
    uploaded the lesson JSON, then the lesson's sounds would not be in the DB, 
    making the lesson useless.

    """
   
     
    def __init__(self, lesson_dict, *args, **kwargs):
        """Create subform instance and populate dictionary w/ given lesson

        :lesson_dict: Dictionary of lesson data (w/ keys lesson_name,
        json_name, activity_type, learning_language, and known_language)

        """
        super(LessonSoundFormset, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

        self.lesson_dict = lesson_dict
        try:
            if "voice" not in self.data: # Django doens't pass empty booleans through POST correctly
                _voice = False
            else:
                _voice = True

            self.subform = LessonDataSubForm(data={
                "speaker_name": self.data["speaker_name"],
                "sex": self.data["sex"],
                "voice": _voice
            })
        except KeyError:
            self.subform = LessonDataSubForm()
            
    def is_valid(self):
        """
        Validate every form in formset and the subform

        Returns True if both the subform and every form in the formset are valid, retun
        False otherwise.
        """
        forms_valid = super(LessonSoundFormset, self).is_valid()
        if self.subform.is_valid():
            return forms_valid
        else:
            return False
    
    def clean(self):
        """Raise validation error if subform is not valid"""
        for field, error in self.subform.errors.items():
            raise forms.ValidationError("{}: {}".format(field, error))

    @property 
    def cleaned_data(self):
        """Return a list of dictionaries of cleaned data for all forms in self.forms and subform"""
        cleaned_data = super(LessonSoundFormset, self).cleaned_data
        cleaned_data.append(self.subform.cleaned_data)
        
        return cleaned_data
    
    def save(self, bucket):
        """Save lesson and sounds to DB. Upload sounds to S3

        Typically formsets don't have a save() method, but we implemented one here
        so that saving the sounds and lesson, and the uploading can all happen at the same
        time

        Args:
            bucket: An instance of boto3 S3.bucket

        """
        saved = {
                "lesson": None,
                "sounds": []
        }
        lesson = Lesson(**self.lesson_dict)
        lesson.save()
        saved["lesson"] = lesson

        # Save sound
        for form in self.forms:
            sound_dict = form.cleaned_data["sound_info"]
            sound = Sound(
                    language = lesson.learning_language,
                    associated_lesson = lesson,
                    speaker_name = self.subform.cleaned_data["speaker_name"],
                    sex = self.subform.cleaned_data["sex"],
                    voice = self.subform.cleaned_data["voice"],
                    **sound_dict
            )
            context = ssl._create_unverified_context()
            sound_data = urlopen(sound.lesson_url, context=context).read()
            sound.sound_hash = sha1(sound_data).hexdigest()
        
            # Upload to S3
            bucket.put_object(Key=sound.key, Body=sound_data, ContentType="audio/x-wav")
            sound.save(bucket) # We pass the bucket argument in case sound already exists, but is being changed
            saved["sounds"].append(sound)

        return saved

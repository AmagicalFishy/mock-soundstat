from django import forms
from hashlib import sha1
from soundtests.models import Lesson, Sound
from urllib.request import urlopen

class LessonSoundForm(forms.Form):
    """Form for each sound in an uploaded lesson

    The only fields the user has to fill out here are num_words and num_syllables. Phrases 1 and 2
    should be populated automatically by the view, as should be the lesson hashes and associated
    URLs. The lesson isn't saved yet, nor are the sounds themselves; that is performed in the 
    LessonSoundFormset

    """

    phrase1 = forms.CharField()
    phrase2 = forms.CharField()
    num_words = forms.IntegerField(min_value=1)
    num_syllables = forms.IntegerField(min_value=1)
    lesson_url = forms.CharField()
    lesson_hash = forms.CharField()

    def __init__(self, *args, **kwargs):
        """Set readonly and hidden fields"""
        super(LessonSoundForm, self).__init__(*args, **kwargs)
        self.fields["lesson_url"].widget = forms.HiddenInput()
        self.fields["lesson_hash"].widget = forms.HiddenInput()
        for field in ["phrase1", "phrase2", "lesson_url", "lesson_hash"]:
            self.fields[field].widget.attrs["readonly"] = True

    def clean(self):
        """Create dictionary of sound's info. after validating data"""
        cleaned_data = super(LessonSoundForm, self).clean()
        try: 
            sound_dict = {
                    "phrase1": self.cleaned_data["phrase1"],
                    "phrase2": self.cleaned_data["phrase2"],
                    "num_words": self.cleaned_data["num_words"],
                    "num_syllables": self.cleaned_data["num_syllables"],
                    "lesson_url": self.cleaned_data["lesson_url"],
                    "lesson_hash": self.cleaned_data["lesson_hash"]
            }
            cleaned_data["sound_info"] = sound_dict
        except KeyError:
            pass

        return cleaned_data



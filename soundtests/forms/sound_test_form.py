from ._filterform import SoundFilterForm
from soundtests.models import Sound, SoundTest, TestPair

from django import forms

class SoundTestForm(SoundFilterForm):
    """Form used when a user creates a new test"""
    name = forms.CharField(required=True)
    description = forms.CharField(
            widget=forms.Textarea, 
            required=True
    )
    master_to_master_test = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        """Ensures that only sounds associated with lessons are returned"""
        super(SoundTestForm, self).__init__(*args, **kwargs)
        for field_name in ['sex', 'language', 'speaker_name', 'voice', 'num_syllables', 'num_words']:
            self.fields[field_name].widget.attrs.update({'onChange': 'filter_sounds(this, lessons=true)'})
        self.fields['unique_phrase'].widget.attrs.update({'onChange': 'filter_sounds(this, lessons=true, recording=true)'})

    def clean(self):
        """Presents custom text for error when there is an already-existing Test Name"""
        cleaned_data = super(SoundTestForm, self).clean()
        try:
            if SoundTest.objects.filter(name=cleaned_data['name']).exists():
                raise forms.ValidationError(
                    "There already exists a test with that name. Please choose a different one. (If\
                    the name you chose conflicts with another, perhaps you need to be more\
                    descriptive in your title.)"
                )
        # If there is a key error, this means that the super.clean() call above did not validate, so
        # the user will be presented with that error message instead. Otherwise we'd be redirected to
        # a Django KeyError page
        except KeyError:
            pass
        return cleaned_data

    def save(self):
        """Creates and saves this test as a master-to-master test

        If a user checks the "master-to-master" box, then the save() method of
        this form should be called. If this test is a master-to-master test, 
        then all the information necessary to save the test is present in this
        form (i.e. - it might as well be a ModelForm). Otherwise, the user 
        must associate sounds to each master sound.
        
        """
        new_test = SoundTest(
                name = self.cleaned_data['name'],
                description = self.cleaned_data['description']
        )
        new_test.save()
        for sound in Sound.objects.filter(pk__in=self.cleaned_data['sounds_to_send']):
            test_pair = TestPair(
                    associated_test=new_test,
                    master_sound=sound,
                    user_sound=sound
            )
            test_pair.save()
            new_test.associated_lessons.add(sound.associated_lesson)
        new_test.save()

        return new_test

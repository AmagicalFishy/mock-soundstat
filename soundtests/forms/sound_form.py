from django import forms
from hashlib import sha1
from soundtests.models.sound import Sound

import os

class SoundForm(forms.ModelForm):
    """Uploads sound files/recordings to DB and S3"""

    sounds_to_upload = forms.FileField(widget=forms.ClearableFileInput(attrs={"multiple": True}))

    class Meta:
        model = Sound
        fields = [
            'phrase1', 'phrase2', 'language', 'num_words', 'num_syllables',\
            'sex', 'voice', 'speaker_name'
        ]

    # We overwrite the clean method to add some custom validation to our file field,
    # populate a dictionary w/ sounds and their hashes, and some other stuff
    def clean(self):
        """Validate sound files and create hashes (used as primary keys)"""
        cleaned_data = super(SoundForm, self).clean()
        cleaned_data["hashed_sounds"] = {}

        #if len(self.files.getlist('sounds_to_upload')) == 0:
        #    raise forms.ValidationError("You must upload or record at least one sound")
        for sound_file in self.files.getlist("sounds_to_upload"):
            # Validate WAV file
            if sound_file.size > 1024*1024:
                raise forms.ValidationError("File {} is too large (> 1 mB)".format(sound_file.name))
            if not sound_file.content_type in ["audio/x-wav", "audio/wav"]:
                raise forms.ValidationError("File {} is not a WAV file".format(sound_file.name))
            if not os.path.splitext(sound_file.name)[1] in [".wav"]:
                raise forms.ValidationError("File {} does not have proper extension (.wav)".format(sound_file.name))

            # Populate cleaned_data with appropriate sounds and their hashes
            sound_data = sound_file.read()
            cleaned_data["hashed_sounds"][sha1(sound_data).hexdigest()] = sound_data

            # Capitalize language
            cleaned_data["language"] = cleaned_data["language"].upper()

        return cleaned_data

    # We overwrite the save() method to require an instance of a boto3 S3 bucket to upload
    # sounds to S3 before saving to database
    def save(self, bucket, commit=True):
        """Saves sounds to S3 and to database

        This is performed in the save method of this form instead of as a
        method of Sound because the form needs to deal with the file-data
        in order to properly upload to S3. 

        Args:
            bucket: An instance of boto3's S3.bucket

        """

        # Upload then save sound for every uploaded file
        saved_sounds = []
        for sound_hash, sound_data in self.cleaned_data["hashed_sounds"].items():
            sound = Sound(
                    phrase1 = self.cleaned_data["phrase1"],
                    phrase2 = self.cleaned_data["phrase2"],
                    language = self.cleaned_data["language"],
                    num_words = self.cleaned_data["num_words"],
                    num_syllables = self.cleaned_data["num_syllables"],
                    sex = self.cleaned_data["sex"],
                    voice = self.cleaned_data["voice"],
                    speaker_name = self.cleaned_data["speaker_name"],
                    sound_hash = sound_hash
            )
            # Save the sound *after* uploading to S3, in case there's ever an
            # error uploading, the sound won't be saved w/o a corresponding S3 file
            bucket.put_object(Key=sound.key, Body=sound_data, ContentType="audio/x-wav")
            sound.save(bucket) # We pass the 'bucket' arg. in case sound exists, but is being changed
            saved_sounds.append(sound)
        return saved_sounds

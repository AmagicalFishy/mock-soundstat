from ._filterform import SoundFilterForm

from django import forms

class AssociateSoundForm(SoundFilterForm):
    """Adds extra fields to SoundFilterForm to pass data

    Since this inherits from :class:`soundtests.forms.SoundFilterForm`, it
    will also require the 'sounds_to_send' parameter; a list of primary
    keys which, in this case, are the sounds to be sent to the sever.
    
    """

    sound_language = forms.CharField(widget=forms.HiddenInput())
    sound_pk = forms.CharField(widget=forms.HiddenInput())
    master_phrase1 = forms.CharField(widget=forms.HiddenInput())
    master_phrase2 = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(AssociateSoundForm, self).__init__(*args, **kwargs)
        self.fields['unique_phrase'].widget.attrs.update({'onChange': 'filter_sounds(this, lessons=false, recording=true)'})

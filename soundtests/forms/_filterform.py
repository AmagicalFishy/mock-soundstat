from django import forms
from soundtests.models.sound import Sound
"""
Defines the base filter form from which all others inherit
"""

def distinct_values(field):
    """Helper function which returns distinct values of the appropriate Sound field"""
    return [[x[field], x[field]] for x in Sound.objects.order_by(field).values(field).distinct()]

# SoundChoiceField and SoundPhraseField are defined to alter how they validate.
# Since the choices in these fields will be altered dynamically, they wouldn't
# validate otherwise, so we check to ensure their values are in the database, 
# regardless of what the initial choices were
class SingleSoundField(forms.ChoiceField):
    """Validates javascript changes in ChoiceField"""

    def __init__(self, field, *args, **kwargs):
        """Def. __init__ to require input of field"""
        super(SingleSoundField, self).__init__(*args, **kwargs)
        self.field = field

    def valid_value(self, value):
        filter_dict = {"{}__exact".format(self.field): value}
        if not Sound.objects.filter(**filter_dict):
            return False
        else:
            return True

class MultiSoundField(forms.MultipleChoiceField):
    """Validates javascript changes in MultipleChoiceField"""

    def __init__(self, field, *args, **kwargs):
        """Def. __init__ to require input of field"""
        super(MultiSoundField, self).__init__(*args, **kwargs)
        self.field = field

    def valid_value(self, value):
        filter_dict = {"{}__exact".format(self.field): value}
        if not Sound.objects.filter(**filter_dict):
            return False
        else:
            return True


class SoundFilterForm(forms.Form):
    """
    Filters Sounds according to user selection

    This form is the search-filter for searching words in the database via
    a few clickable parameters. The associated javascript functions 
    called in onChange are located at /static/jscript_functions.js
    """

    # Allows user to filter by sex
    sex = forms.MultipleChoiceField(
            choices=Sound.SEX_CHOICES,
            widget=forms.SelectMultiple(
                attrs={"onChange":"filter_sounds(this)"}
            ),
            required=False
    )

    # Allows user to filter by language
    language = forms.MultipleChoiceField(
            widget=forms.SelectMultiple(
                attrs={"onChange":"filter_sounds(this)"}
            ),
            required=False
    )

    # Allows user to filter by whether or not voice is synthetic
    voice = forms.MultipleChoiceField(
            widget=forms.SelectMultiple(
                attrs={"onChange":"filter_sounds(this)"}
            ),
            required=False
    )

    # Allows user to filter by number of words
    num_words = forms.MultipleChoiceField(
            widget=forms.SelectMultiple(
                attrs={"onChange":"filter_sounds(this)"}
            ),
            required=False
    )

    # Allows user to filter by number of syllables 
    num_syllables = forms.MultipleChoiceField(
            widget=forms.SelectMultiple(
                attrs={"onChange":"filter_sounds(this)"}
            ),
            required=False
    )

    # Allows user to filter by speaker name
    speaker_name = forms.MultipleChoiceField(
            widget=forms.SelectMultiple(
                attrs={"onChange":"filter_sounds(this)"}
            ),
            required=False
    )

    # Displays the unique phrases which adhere to above filters (i.e. - If
    # there are 30 recordings of the word "dog," then we will see only one
    # entry for "dog")
    unique_phrase = MultiSoundField(
            field="phrase1",
            widget=forms.SelectMultiple(
                attrs={
                    "style":"width: 100%", 
                    "onChange":"filter_sounds(this, unique=true)",
                    'size':10
                }
            ),
            required=False
    )
   
    # Displays the recordings available to send to the server; 
    # double-clicking on the choices in this form will play the sound
    sounds_to_send = MultiSoundField(
            field="pk",
            widget=forms.SelectMultiple(
                attrs={
                    "style":"width: 100%",
                    'size':10
                }
            )
    )

    # We must override the __init__ method so that the Language choices update
    # whenever we add sounds w/ a new language
    def __init__(self, *args, **kwargs):
        """
        __init__ method which forces update on change

        The available choices in this form aren't static; they update
        according to whatever is in the database.

        """

        super(SoundFilterForm, self).__init__(*args, **kwargs)
        for field in ['language', 'speaker_name', 'num_words', 'num_syllables']:
            self.fields[field].choices = distinct_values(field)
        self.fields['voice'].choices = [[False, "Natural"], [True, "Synthetic"]]


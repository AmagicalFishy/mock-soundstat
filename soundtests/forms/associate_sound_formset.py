from django import forms
from soundtests.models import Sound, SoundTest, TestPair

class AssociateSoundFormset(forms.BaseFormSet):
    """Formset for each sound in a created lesson"""
     
    def __init__(self, test_data, *args, **kwargs):
        """Store test information and require all forms to be filled out

        Args:
            test_data (dict): A dictionary with two keys, "name" and
            "description" whose values are the name and description of the test
            respectively. We don't worry about verifying contents of test_data 
            here; they are passed through from :ref:`soundtests.views.create_test`,
            which has already verified them.
        
        """
        super(AssociateSoundFormset, self).__init__(*args, **kwargs)
        self.test_name = test_data['name']
        self.test_description = test_data['description']
        for form in self.forms:
            form.empty_permitted = False

    def save(self):
        """Save the :class:`SoundTest` and all :class:`TestPair`s
        
        We save all of the appropriate model objects last, and associate
        with the test all of the lessons from which the master sounds come.
        
        """
        # Create test
        new_test = SoundTest(name=self.test_name, description=self.test_description)
        new_test.save()

        # Save sound
        for form in self.forms:
            master_sound = Sound.objects.get(pk=form.cleaned_data['sound_pk'])
            for pk in form.cleaned_data['sounds_to_send']:
                user_sound = Sound.objects.get(pk=pk)
                test_pair = TestPair(
                        associated_test=new_test,
                        master_sound=master_sound,
                        user_sound=user_sound
                )
                test_pair.save()
                new_test.associated_lessons.add(master_sound.associated_lesson)
        new_test.save()
        return new_test

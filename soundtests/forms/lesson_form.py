from django import forms
from hashlib import sha1
from soundtests.models.lesson import Lesson

import json
import os

class LessonForm(forms.Form):
    """This is the form that's used when a user uploads a lesson's JSON
    
    It is a simple form UI-wise, but must validate the JSON to make sure 
    it has all the appropriate keys (basically, to make sure it's actually 
    a lesson JSON). It populates cleaned_data with the lesson JSON so that
    the appropriate info. can be passed onto the classify-sound forms.

    """
    lesson_name = forms.CharField(required=True)
    lesson_json_file = forms.FileField(required=True)

    def clean(self):
        cleaned_data = super(LessonForm, self).clean()

        # Check if file uploaded is valid JSON
        try:
            # Add JSON to variable which will replace "cleaned_data"
            lesson_json = json.loads(cleaned_data['lesson_json_file'].read().decode("utf-8")) 
        except ValueError:
            raise forms.ValidationError('This is not valid JSON')

        # Check to see if JSON structure matches a lesson's JSON structure
        needed_fields_lesson = ['listName', 'languageLearn', 'languageNative', 'files', 'os',
                'userName', 'activityType']
        for field in needed_fields_lesson:
            if field not in list(lesson_json.keys()):
                raise forms.ValidationError(
                    "This is not valid Lesson JSON. You're missing the field " + field
                )

        # Check to see if each sound JSON structure matches what a lesson's sound JSON structure is
        needed_fields_sound = ['url', 'priority', 'soundData', 'hash', 'phrase1', 'phrase2',\
                'phraseTranslit', 'learningLanguageCode', 'knownLanguageCode', 'precompareScore']
        for field in needed_fields_sound:
            for sound in lesson_json['files']:
                if field not in list(sound.keys()):
                    raise forms.ValidationError(
                            "This is not valid Lesson JSON. One of your sounds is missing the " +
                            field + " field."
                    )
        # Check to see if lesson already exists in database
        try: 
            Lesson.objects.get(lesson_name = cleaned_data['lesson_name'])
            raise forms.ValidationError("You've already created a lesson with this name.")

        except Lesson.DoesNotExist:
            lesson_json.update({'user_created_name': cleaned_data['lesson_name']})

        # If a key error is thrown, this means that the super.clean call above
        # did not validate, so there is no 'lesson_name' key; when the user presses
        # the submit button, the error in said clean() call will be shown
        except KeyError:
            pass

        cleaned_data = lesson_json
        
        return cleaned_data


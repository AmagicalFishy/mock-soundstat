from django.apps import AppConfig


class SoundtestsConfig(AppConfig):
    name = 'soundtests'

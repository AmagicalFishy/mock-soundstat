from django.http import JsonResponse
from soundtests.models import Sound

def get_url(request):
    """Sends back sound's URL on AJAX request. Used w/ javascript function 'play_sound()' """
    if request.is_ajax() and request.method == 'GET':
        sound = Sound.objects.get(pk=request.GET.get('pk'))
        return JsonResponse({'url': sound.url})

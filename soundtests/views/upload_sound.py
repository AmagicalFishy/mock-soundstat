from django.shortcuts import render
from soundtests.forms import SoundForm
from utils import S3Connection


def upload_sound(request):
    """View used to process user uploading sound.

    For every phrase a user uploads, said user must classify said phrase. The
    sound's primary key is its SHA-1 hash (ensuring sounds aren't duplicated
    in the database) and the .wav selected by the user is uploaded to Amazon

    """

    if request.method == 'POST':
        form = SoundForm(request.POST, request.FILES)
        if form.is_valid():
            s3 = S3Connection()
            form.save(bucket=s3.bucket)
    else:
        form = SoundForm()
    return render(request, 'soundtests/upload_sound.html', {'form': form})

from django.shortcuts import redirect, render
from soundtests.forms import LessonForm

def upload_lesson(request):
    """Displays form Used when user uploads lesson JSON"""
    if request.method == "POST":
        form = LessonForm(request.POST, request.FILES)
        if form.is_valid():
            request.session["cleaned_data"] = form.cleaned_data
            return redirect("soundtests:upload_lesson_sounds")

    else:
        form = LessonForm()
    return render(request, "soundtests/upload_lesson.html", {"form": form})


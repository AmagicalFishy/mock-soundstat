from django.forms import formset_factory
from django.shortcuts import render, redirect

from soundtests.forms import AssociateSoundForm, AssociateSoundFormset
from soundtests.models import Sound

def associate_sounds(request):
    """Called after a user chooses name, description, and lesson when creating a test
    
    This either calls the save() method on the form (in the case that
    master-to-master is checked, or redirects the user to the page where each
    master sound in the test can be directed to a set of user sounds

    """
    AssociateSoundFormset_factory = formset_factory(
            form=AssociateSoundForm,
            formset=AssociateSoundFormset,
            extra=0,
    )
    test_data = {
            'name': request.session['test_name'], 
            'description': request.session['test_description']
    }

    # User was redirected
    if request.method == 'GET':
        sound_info = []                                                 
        for sound_key in request.session['test_keys']:    
            sound = Sound.objects.get(pk=sound_key)                     
            tmp_info = {                                                
                'sound_language': sound.language,                       
                'sound_pk': sound.pk,                                   
                'master_phrase1': sound.phrase1,                        
                'master_phrase2': sound.phrase2,                        
            }                                                           
            sound_info.append(tmp_info)                                 
        formset = AssociateSoundFormset_factory(
                initial=sound_info,
                test_data=test_data
        ) 
    
    # User submitted this form
    if request.method == 'POST':
        formset = AssociateSoundFormset_factory(
                data=request.POST, 
                test_data=test_data
        )

        if formset.is_valid():
            formset.save()
            return redirect('soundtests:index')

    return render(request, 'soundtests/associate_sounds.html', {'formset': formset})

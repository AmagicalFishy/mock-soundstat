# -*- coding: utf-8 -*-

import json
import os
import logging
import wave
import tempfile
import wavio
import urllib
import requests

from numpy import array
from hashlib import sha1
from binascii import b2a_hex

from django import forms
from django.conf import settings
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.forms import formset_factory
from django.core.files.uploadedfile import InMemoryUploadedFile

from common.utils.amazon_s3 import S3Connection, generate_key, generate_url

from soundtests.models.sound import Sound, Task
from soundtests.models.lesson import Lesson
from soundtests.models.testmodels import TestPair, SoundTest
from soundtests.forms import SoundForm, SoundUploadForm, RecordSoundForm, LessonForm, LessonSoundForm
from soundtests.forms import SoundTestForm, AssociateSoundForm, MasterSoundForm

# Logger
logger = logging.getLogger('standard_logger')

#cert = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, "common", "transparent-test-network.pem"))
def index(request):
    """
    View which renders the homepage of the Sound Testing UI. This doesn't really do anything
    outside of fetching whatever 'Task' objects which don't have 100% completion and sending
    them to the HTML template (so they can be printed out as a table).
    """

    logger.info("Beginning home-page render (getting task info.)")
    task_list = []  # This will be passed to the template
    tasks = Task.objects.all().exclude(progress=100)
    for task in tasks:
        task.timed_delete() # Attempt to cleanup tasks w/e user accesses page
        task.save()
        task_list.append([task.get_task_display(), task.tasked_with, task.progress])
    if len(task_list) == 0:
        task_list.append(["None", "--", "--"])

    logger.info("Sending task info. to HTML template")
    return render(request, 'soundtests/index.html', {'task_list': task_list})


def upsound(request):
    """
    View used to process user uploading sound. For every sound a user uploads, said user must
    classify the sound. The sound's primary key is its SHA-1 hash (ensuring sounds aren't
    duplicated in the database) and the .wav selected by the user is uploaded to Amazon
    """
    available_masters = RecordSoundForm(request.POST, auto_id="id_form-0-%s")
    if request.method == 'POST':
        logger.info("Request method is POST; initializing Sound modelform w/ provided data")
        # Initialize with fake sound hash, since sound_hash is a required field
        form = SoundUploadForm(request.POST, request.FILES, instance=Sound(sound_hash="0A"))
        logger.info("Validating form...")
        if form.is_valid():
            sounds = form.save()
            logger.info("Saving sounds to DB...") 
            # Open an S3 session and upload each sound to its
            # respective sub-folder
            logger.info("Creating S3 connection...")
            s3_connection = S3Connection()
            logger.info("Retrieving S3 bucket...")
            s3_bucket = s3_connection.bucket
            for sound, file_contents in sounds:
                # Store in bucket like: sa_soundstat/sounds/[language]/[phrase]/[hash].wav
                logger.info('Uploading sound "%s" to S3...', sound.phrase2)
                s3_bucket.put_object(Key=sound.key, Body=file_contents, ContentType='audio/x-wav')
                logger.info('Successfully uploaded "%s" to S3!', sound.phrase2)
            logger.info("Sounds uploaded and saved!")
    else:
        form = SoundUploadForm()
    return render(request, 'soundtests/upsound.html', {'form': form, 'available_masters': available_masters})

def recsound(request):
    return render(request, 'soundtests/recsound.html')

def uplesson(request):
    """
    View used when user uploads lesson JSON. Most of the work is done in the form definition;
    which is just validating the lesson JSON, making sure it looks the way it's supposed to,
    etc.
    """
    if request.method == 'POST':
        logger.info("Request method is POST, creating form with submitted data...")
        form = LessonForm(request.POST, request.FILES)
        logger.info("Validating form...")
        if form.is_valid():
            request.session['cleaned_data'] = form.cleaned_data  # Add validated data to session
            return redirect('soundtests:uplesson_sounds')

    else:
        form = LessonForm()
    return render(request, 'soundtests/uplesson.html', {'form': form})


def uplesson_sounds(request):
    """
    View used where user fills in information about each sound from the sounds in a
    Lesson's JSON.
    """

    # If user didn't come to this page through submitting a lesson, send them back!
    if 'cleaned_data' not in request.session:
        return HttpResponse(
            'BACK WHENCE THOUST CAME, CREATURE!! THOU CANNAE GAIN ENTRANCE HERE<br>'
            'LONG HAVE THE WRETCHED THREE TOLD OF THINE ADVENT<br>'
            'LONG HAVE I TOILED TO BUILD THIS GATE TO PREVENT THINE MEDDLING<br>'
            'AND NOW THAT THIS TIME HATH COMETH<br>'
            'IT IS MY QUEST AND MINE ALONE TO ENSURE THOU SHANT ENTER HERE<br>'
            'SO I SAY TO THEE ONCE MORE AND ONCE MORE ONLY<br>'
            'BACK!!'
        )

    # Grab/store information collected when user uploaded lesson
    # and put in all info. for lesson's master sounds
    sound_info = []
    # Grab all the data already in the lesson JSON
    for sound in request.session['cleaned_data']['files']:
        logger.info('Phrase: ' + sound['phrase1'])
        tmp_info = {
            'phrase1': sound['phrase1'],
            'phrase2': sound['phrase2'],
            'language': sound['learningLanguageCode'],
            'url': sound['url'],
            'lesson_hash': sound['hash'],
            'sound_hash': str(b2a_hex(os.urandom(20))),
            'lesson': request.session['cleaned_data']['listName'],
        }
        sound_info.append(tmp_info)
        logger.info('Sound hash: ' + tmp_info['sound_hash'])
        logger.info('Lesson hash: ' + tmp_info['lesson_hash'])

    LessonSoundFormSet = formset_factory(LessonSoundForm, extra=0)

    if request.method == 'POST':
        formset = LessonSoundFormSet(request.POST)
        if formset.is_valid():
            # Actually create lesson (whose data has already been validated)
            lesson = Lesson(
                lesson_name = request.session['cleaned_data']['user_created_name'],
                json_name = request.session['cleaned_data']['listName'],
                activity_type = request.session['cleaned_data']['activityType'],
                learning_language = request.session['cleaned_data']['languageLearn'],
                known_language = request.session['cleaned_data']['languageNative']
            )
            lesson.save()

            # Save all Sound objects
            for form in formset:
                sound = form.save(lesson_pk=lesson.pk)

                # Fix sound hash to reflect actual SHA1 sound hash instead of random char.
                sound.sound_hash = sha1(sound.get_sound_from_url()).hexdigest()
                sound.key = generate_key(sound)
                sound.save()

                # Upload sound to S3
                logger.info('Fetching sound file from lesson URL for sound "%s"...', sound.phrase2)
                sound_file = sound.get_sound_from_url()
                logger.info("Creating S3 connection...")
                s3_connection = S3Connection()
                logger.info("Retrieving S3 bucket...")
                s3_bucket = s3_connection.bucket
                logger.info("Uploading sound %s to S3...", sound.phrase2)
                s3_bucket.put_object(Key=sound.key, Body=sound_file, ContentType='audio/x-wav')
                logger.info("Sounds uploaded!")

            del request.session['cleaned_data']
            return redirect('soundtests:index') # Back to home page

        # If form didn't validate, come back to this same page
        return render(
            request,
            'soundtests/uplesson_sounds.html',
            {
                'formset': formset,
                'lesson': request.session['cleaned_data']['user_created_name'],
                'list': request.session['cleaned_data']['listName'],
            }
        )
    # If request method isn't POST, then the user isn't submitting a form; said
    # user is being redirected from uploading a lesson; send appropriate info to
    # template
    else:
        formset = LessonSoundFormSet(initial=sound_info)
        return render(
            request,
            'soundtests/uplesson_sounds.html',
            {
                'formset': formset,
                'lesson': request.session['cleaned_data']['user_created_name'],
                'list': request.session['cleaned_data']['listName'],
                'num_words': len(request.session['cleaned_data']['files']),
                'language': request.session['cleaned_data']['languageLearn']
            }
        )


def create_test(request):
    """
    View called when user clicks 'Create New Test'. SoundTestForm is a simple form
    with three inputs. If the form validates, then user is redirected to
    the 'associate_sounds' function
    """

    if request.method == 'POST':
        form = SoundTestForm(request.POST, auto_id="id_form-0-%s")
        if form.is_valid():
            if request.POST.get("master_to_master"): # If this is a master-to-master test
                # Create and save test
                new_test = SoundTest(
                    name = form.cleaned_data['name'],
                    description = form.cleaned_data['description']
                )
                new_test.save()
                
                # Associate lessons w/ test
                for sound in Sound.objects.filter(pk__in=request.POST.getlist('master_sounds[]')):
                    logger.info
                    test_pair = TestPair(
                            associated_test=new_test,
                            master_sound=sound,
                            user_sound=sound
                    )
                    test_pair.save()
                    new_test.associated_lessons.add(sound.lesson)
                    new_test.save()
                    
                return redirect('soundtests:index')

            else:
            # Add test data to session and redirect to sound association
                request.session['test_data'] = {}
                request.session['test_data']['name'] = form.cleaned_data['name']
                request.session['test_data']['description'] = form.cleaned_data['description']
                request.session['test_data']['sound_keys'] = [pk for pk in request.POST.getlist('master_sounds[]')]

                return redirect('soundtests:associate_sounds')

    else:
        form = SoundTestForm(auto_id="id_form-0-%s")
    return render(request, 'soundtests/create_test.html', {'form': form})


def associate_sounds(request):
    """
    View called after a user chooses name, description, and lesson when creating a test.
    When a user appropriately fills out all of the forms, this creates the test instance
    associates the lessons with the test, creates all of the pairs to test on, and
    associates said pairs with the test instance
    """

    # Stores info. from the lessons to test (so that each bit of info. can
    # be associated w/ the form).
    AssociateSoundFormSet = formset_factory(AssociateSoundForm, extra=0)

    if request.method == 'POST':
        formset = AssociateSoundFormSet(request.POST)

        if formset.is_valid():
            # Create, save, and associate lessons with new test instance
            new_test = SoundTest(
                name = request.session['test_data']['name'],
                description = request.session['test_data']['description']
            )
            new_test.save()

            # Create test pair instance for every user_sound + master_sound pair
            for form in formset:
                master_sound = Sound.objects.get(pk=form.cleaned_data['sound_pk'])
                for sound in Sound.objects.filter(pk__in=form.cleaned_data['sounds_to_send']):
                    test_pair = TestPair(
                        associated_test=new_test,
                        master_sound=master_sound,
                        user_sound=sound
                    )
                    test_pair.save()
                    new_test.associated_lessons.add(master_sound.lesson)
            new_test.save()

            return redirect('soundtests:index')

    # User is being redirected
    else:
        sound_info = []
        for sound_key in request.session['test_data']['sound_keys']:
            sound = Sound.objects.get(pk=sound_key)
            tmp_info = {
                'sound_language': sound.language, 
                'sound_pk': sound.pk,
                'master_phrase1': sound.phrase1,
                'master_phrase2': sound.phrase2,
                'url': sound.url,
            }
            sound_info.append(tmp_info)
        formset = AssociateSoundFormSet(initial=sound_info)

    return render(request, 'soundtests/associate_sounds.html', {'formset': formset})

def view_tests(request): 
    tests = SoundTest.objects.all()
    return render(request, 'soundtests/viewtests.html', {'tests': tests})

def test_contents(request, test_key):
    test = SoundTest.objects.get(pk=test_key)
    lessons = test.associated_lessons.all()
    test_pairs = test.pairs_to_test.all()

    return render(request, 'soundtests/viewtestcontents.html', {'test': test, 'lessons': lessons, 'test_pairs': test_pairs})

def view_lessons(request):
    lessons = Lesson.objects.all()
    return render(request, 'soundtests/viewlessons.html', {'lessons': lessons})

def lesson_contents(request, lesson_key):
    lesson = Lesson.objects.get(pk=lesson_key)
    words = lesson.sounds.all()
    return render(request, 'soundtests/viewlessoncontents.html', {'lesson': lesson, 'words': words})

def sound_filter(request):
    """
    This view's sole purpose is to deal with the AJAX requests that are sent by the
    SoundFilterForm. To add another filter, simply add another if clause w/ the appropiate
    list sent when the user clicks on a particular attribute in the form
    """

    # Checks for AJAX call - When user clicks one of the 3 filters, return
    # the list of unique phrases that match these filters
    if request.is_ajax():
        sounds = Sound.objects.all()
        search_values = list(request.GET.keys())
        lesson_filter = request.GET.get('only_sounds_in_lessons')
        recording_filter = request.GET.get('recording')
        if 'sex[]' in search_values:
            sounds = sounds.filter(sex__in = request.GET.getlist('sex[]'))

        if 'starts_with[]' in search_values:
            sounds = sounds.filter(starts_with__in = request.GET.getlist('starts_with[]'))

        if 'language[]' in search_values:
            sounds = sounds.filter(language__in = request.GET.getlist('language[]'))

        if 'speaker[]' in search_values:
            sounds = sounds.filter(speaker_name__in = request.GET.getlist('speaker[]'))

        if 'voice[]' in search_values:
            sounds = sounds.filter(voice__in = request.GET.getlist('voice[]'))

        if 'num_syllables[]' in search_values:
            sounds = sounds.filter(num_syllables__in = request.GET.getlist('num_syllables[]'))

        if 'num_words[]' in search_values:
            sounds = sounds.filter(num_words__in = request.GET.getlist('num_words[]'))

        if lesson_filter == 'true':
            sounds = sounds.filter(lesson__isnull=False)

        if recording_filter == 'false':
            phrases = []
            unique_phrase_json = [] 
            for sound in sounds:
                if sound.phrase1 not in phrases:
                    phrases.append(sound.phrase1)
                    unique_phrase_json.append({'phrase1': sound.phrase1, 'phrase2': sound.phrase2})

            unique_phrase_json = sorted(unique_phrase_json, key=lambda k: k['phrase2'])
            return HttpResponse(json.dumps(unique_phrase_json))

    # Checks for AJAX call - When user clicks one of the unique phrases, return a list of all
    # sounds which match that phrase
        elif recording_filter == 'true':
            recordings = []
            sounds = sounds.filter(phrase1__in=request.GET.getlist('unique_phrase[]'))
            for count, sound in enumerate(sounds):
                recordings.append({
                    'key': sound.pk,
                    'display': "Recording #" + str(count + 1) + " - ",
                    'phrase1': sound.phrase1,
                    'phrase2': sound.phrase2,
                    'language': sound.language
                })
            return HttpResponse(json.dumps(recordings))

def get_url(request):
    if request.is_ajax():
        sound_pk = request.GET.get('key')
        sound = Sound.objects.get(pk=sound_pk)
        sound_url = generate_url(key=sound.key)
        return HttpResponse(json.dumps({'url': sound_url}))
    else:
        return HttpResponse("Don't do that. >:(")

def fill_sound_form(request):
    if request.is_ajax():
        key = request.GET.get('sound_pk')
        sound = Sound.objects.get(pk=key)
        form_info = {
                'id_phrase1': sound.phrase1,
                'id_phrase2': sound.phrase2,
                'id_language': sound.language,
                'id_num_words': sound.num_words,
                'id_num_syllables': sound.num_syllables,
                'id_starts_with': sound.starts_with,
                'id_voice': sound.voice
        }
        return HttpResponse(json.dumps(form_info))
    else:
        return HttpResponse(":(")

def save_recording(request):
    if request.is_ajax() and request.method == 'POST':
        form = SoundForm(request.POST)
        if form.is_valid():
            with tempfile.NamedTemporaryFile() as sound_recording:
                #sound_array_string = request.POST.get('audio')
                #sound_array = array([float(x) for x in sound_array_string.split(',')])
                #wavio.write(sound_recording, sound_array, 48000, sampwidth=2)
                #sound_recording.seek(0)
                #wav_file = sound_recording.read()
                #sound_recording.seek(0)
                wav_file = request.FILES['audio'].read()
                sound_instance = form.save(commit=False)
                sound_instance.sound_hash = sha1(wav_file).hexdigest()
                sound_instance.key = generate_key(sound_instance)
                logger.info("Creating S3 connection...")
                s3_connection = S3Connection()
                logger.info("Retrieving S3 bucket...")
                s3_bucket = s3_connection.bucket
                # Store in bucket like: sa_soundstat/sounds/[language]/[phrase]/[hash].wav
                logger.info('Uploading sound "%s" to S3...', sound_instance.phrase2)
                s3_bucket.put_object(Key=sound_instance.key, Body=wav_file, ContentType='audio/x-wav')
                logger.info('Successfully uploaded "%s" to S3!', sound_instance.phrase2)
                logger.info("Saving sound...")
                sound_instance.save()
                logger.info("Sounds uploaded and saved!")
                logger.info("Sound hash: %s", sound_instance.sound_hash)
            return HttpResponse('success')
        else:
            return HttpResponse(form.errors.as_json())

        
    else:
        return HttpResponse("YOU DON'T BELONG HERE!!") 

def download(request, path):
    file_path = os.path.join(settings.STATIC_ROOT, path)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="text/plain")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    else:
        return HttpResponse(file_path)

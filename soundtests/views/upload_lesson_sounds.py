from django.forms import formset_factory
from django.shortcuts import redirect, render
from soundtests.forms import LessonSoundForm, LessonSoundFormset
from utils import S3Connection

def upload_lesson_sounds(request):
    """Handle user classification of lesson sounds
   
    For each sound in the lesson, the user must classify this sound
    accordingly (which is, basically, determining how many words and how many
    syllables). Upon clicking submit, for every sound in the lesson, a
    :class:`main.models.sound.Sound` is created.

    """

    # If user didn't come to this page through submitting a lesson, send them back!
    if 'cleaned_data' not in request.session:
        return redirect("soundtests:upload_lesson")
   
    LessonSoundFormset_factory = formset_factory(
            form=LessonSoundForm, 
            formset=LessonSoundFormset,
            extra=0
    )

    # Create dictionary array of sounds to pass onto formset
    sound_info = []
    for sound in request.session["cleaned_data"]["files"]:
        tmp_info = {
            "phrase1": sound["phrase1"],
            "phrase2": sound["phrase2"],
            "lesson_url": sound["url"],
            "lesson_hash": sound["hash"]
        }
        sound_info.append(tmp_info)

    # Create dictionary w/ info. from lesson, which is passed through from the lessonform
    # cleaned_data, then to this page via the session variable
    lesson_dict = {
            "lesson_name": request.session["cleaned_data"]["user_created_name"],
            "json_name": request.session["cleaned_data"]["listName"],
            "activity_type": request.session["cleaned_data"]["activityType"],
            "learning_language": request.session["cleaned_data"]["languageLearn"],
            "known_language": request.session["cleaned_data"]["languageNative"]
    }

    if request.method == "POST":
        formset = LessonSoundFormset_factory(
                data=request.POST,
                initial=sound_info,
                lesson_dict=lesson_dict
        )
        if formset.is_valid():
            s3 = S3Connection()
            formset.save(bucket=s3.bucket)
            return redirect("soundtests:index")
        return render(request, "soundtests/upload_lesson_sounds.html", {"formset": formset})

    else: 
        formset = LessonSoundFormset_factory(
                initial=sound_info,
                lesson_dict=lesson_dict
        )
        return render(request, "soundtests/upload_lesson_sounds.html", {"formset": formset})

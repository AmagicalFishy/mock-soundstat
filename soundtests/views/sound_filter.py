from django.http import HttpResponse
from soundtests.models.sound import Sound

import json

def sound_filter(request):
    """Deals with AJAX requests sent by the SoundFilterForm

    To add another filter, simply add another if clause w/ the appropiate list
    sent when the user clicks on a particular attribute in the form:

    if 'new_parameter[]' in search_values:
        sounds = sounds.filter(new_parameter__in = request.GET.getlist('new_parameter[]'))

    Don't forget to add to the 'data' sent in the Ajax request defined in
    :ref:`static.javascript.filter_sounds.js`
    
    All of the sounds filtered are ultimately passed to unique_phrase_json, 
    which is what is returned to the javascript function (and displayed on
    the resulting form to the user)

    """

    # Checks for AJAX call - When user clicks one of the 3 filters, return
    # the list of unique phrases that match these filters
    if request.is_ajax():
        sounds = Sound.objects.all()
        search_values = list(request.GET.keys())
        lesson_filter = request.GET.get("lessons")
        recording_filter = request.GET.get('recording')
        if 'sex[]' in search_values:
            sounds = sounds.filter(sex__in = request.GET.getlist('sex[]'))

        if 'language[]' in search_values:
            sounds = sounds.filter(language__in = request.GET.getlist('language[]'))

        if 'speaker[]' in search_values:
            sounds = sounds.filter(speaker_name__in = request.GET.getlist('speaker[]'))

        if 'voice[]' in search_values:
            sounds = sounds.filter(voice__in = request.GET.getlist('voice[]'))

        if 'num_syllables[]' in search_values:
            sounds = sounds.filter(num_syllables__in = request.GET.getlist('num_syllables[]'))

        if 'num_words[]' in search_values:
            sounds = sounds.filter(num_words__in = request.GET.getlist('num_words[]'))

        if lesson_filter == 'true':
            sounds = sounds.filter(associated_lesson__isnull=False)

    # Checks for AJAX call - When user clicks one of the unique phrases, return a list of all
    # sounds which match that phrase
        if recording_filter == 'true':
            recordings = []
            sounds = sounds.filter(phrase1__in=request.GET.getlist('unique_phrase[]'))
            for count, sound in enumerate(sounds):
                recordings.append({
                    'key': sound.pk,
                    'display': "Recording #" + str(count + 1) + " - ",
                    'phrase1': sound.phrase1,
                    'phrase2': sound.phrase2,
                    'language': sound.language
                })
            return HttpResponse(json.dumps(recordings))

        else:
            phrases = []
            unique_phrase_json = [] 
            for sound in sounds:
                if sound.phrase1 not in phrases:
                    phrases.append(sound.phrase1)
                    unique_phrase_json.append({'phrase1': sound.phrase1, 'phrase2': sound.phrase2})

            unique_phrase_json = sorted(unique_phrase_json, key=lambda k: k['phrase2'])
            return HttpResponse(json.dumps(unique_phrase_json))


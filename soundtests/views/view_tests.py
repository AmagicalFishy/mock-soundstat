from django.views.generic import ListView
from soundtests.models import SoundTest

class ViewTests(ListView):
    """Lists all available tests

    When a user clicks the "View Test Contents" link on the homepage, this view
    is called.
    """
    context_object_name = 'soundtests'
    model = SoundTest
    paginate_by = 10
    template_name = 'soundtests/view_tests.html'

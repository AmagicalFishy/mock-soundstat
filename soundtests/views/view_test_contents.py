from django.views.generic import DetailView
from soundtests.models import SoundTest

class ViewTestContents(DetailView):
    context_object_name = 'test'
    model = SoundTest
    slug_url_kwarg = 'test_slug'
    template_name = 'soundtests/view_test_contents.html'


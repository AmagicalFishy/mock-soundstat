from django.shortcuts import render, redirect
from soundtests.forms import SoundTestForm

def create_test(request):
    """Displays form to create a test"""
   
    if request.method == 'POST':
        form = SoundTestForm(request.POST, prefix='form-0')
        if form.is_valid():
            if request.POST.get('form-0-master_to_master_test'):
                form.save()
                return redirect('soundtests:index')

            else:
                # If this is not a master-to-master test, send user to associate
                # sounds page
                request.session['test_name'] = form.cleaned_data['name']
                request.session['test_description'] = form.cleaned_data['description']
                request.session['test_keys'] = [pk for pk in request.POST.getlist('master_sounds[]')]

                return redirect('soundtests:associate_sounds')
    else:
        form = SoundTestForm(prefix='form-0') # The javascript necessitates this prefix
    return render(request, 'soundtests/create_test.html', {'form': form})

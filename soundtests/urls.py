from django.urls import path
from soundtests import views

app_name = 'soundtests'

urlpatterns = [
    # Sound Analysis UI's Homepage 
    path('', views.index, name='index'),
    
    # Views where user uploads
    path('upload_sounds', views.upload_sound, name='upload_sound'),
    path('upload_lesson', views.upload_lesson, name='upload_lesson'),
    path('upload_lesson/sounds', views.upload_lesson_sounds, name='upload_lesson_sounds'),

    # Views that work w/ creating tests
    path('create_test', views.create_test, name='create_test'),
    path('associate_sounds', views.associate_sounds, name='associate_sounds'),

    # Views that display contents
    path('view_tests', views.ViewTests.as_view(), name='view_tests'),
    path('view_tests/<test_slug>', views.ViewTestContents.as_view(), name='view_test_contents'),

    # AJAX views
    path('sound_filter', views.sound_filter, name='sound_filter'),
    path('get_url', views.get_url, name='get_url'),
]

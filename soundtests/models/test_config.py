from django.db import models
from soundtests.models import SoundTest

class TestConfig(models.Model):
    """Stores the server configuration for a particular test run
    
    Each of these is associated with one (and only one) 
    :class:`soundtests.models.SoundTest`. Every time a test is run, one of
    these is created and associated with it. 

    """

    SERVER_CHOICES = (
            ("[REDACTED]", "Live Server"),
            ("[REDACTED]", "Nashua Test Server"),
            ("[REDACTED]", "AWS Test Server")
    )

    SCORING_CHOICES = (
            ("pro", "Pronunciation"),
            ("spv", "SPV")
    )

    associated_test = models.ForeignKey(SoundTest, on_delete=models.SET_NULL, related_name="runs", null=True)
    jsession_id = models.CharField(max_length=32, default="No ID Assigned Yet")
    date = models.DateTimeField(editable=False, null=True, default=None)
    gain = models.PositiveSmallIntegerField(default=1)
    vad_start = models.PositiveSmallIntegerField(default=0)
    user_sound_snr = models.PositiveSmallIntegerField(default=25)
    added_silence = models.BooleanField(default=True)
    silence_before = models.PositiveSmallIntegerField(default=1)
    silence_after = models.PositiveSmallIntegerField(default=1)
    random_snr = models.BooleanField(default=True, verbose_name="Random SNR")
    server = models.CharField(max_length=50, choices=SERVER_CHOICES)
    scoring = models.CharField(max_length=25, choices=SCORING_CHOICES, default='pro')
    server_version = models.CharField(max_length=100)
    success = models.BooleanField(default=False)

    def __str__(self):
        if self.date is None:
            config_title = "NULL"
        else:
            try:
                config_title = self.associated_test.name + " on " + str(self.date)
            except AttributeError:
                config_title = "Associated Test Deleted" 
        return config_title

    def snr(self):
        if self.random_snr:
            return randint(15, 30)
        return self.user_sound_snr

    class Meta:
        db_table = 'test_configs'
        ordering = ['-date']

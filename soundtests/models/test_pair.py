from django.db import models
from soundtests.models import Sound, SoundTest

class TestPair(models.Model):
    """Stores which user sounds are associated to which master sounds

    Each :class:`SoundTest` is associated with many of these.
    Running a test effectively consists of running through each associated 
    TestPair and comparing the defined user sound to the defined master sound.

    """
    
    associated_test = models.ForeignKey(SoundTest, on_delete=models.SET_NULL, editable=False, related_name='pairs_to_test', null=True, default=None)
    master_sound = models.ForeignKey(Sound, on_delete=models.CASCADE, related_name="used_as_master")
    user_sound = models.ForeignKey(Sound, on_delete=models.CASCADE, related_name="used_as_user", null=True, default=None)

    def __str__(self):
        pair_name = self.associated_test.name + ": " + self.user_sound.phrase2 + " -> " + self.master_sound.phrase2

        return pair_name

    class Meta:
        db_table = 'test_pairs'

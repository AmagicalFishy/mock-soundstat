from django.db import models
from django.template.defaultfilters import slugify
from soundtests.models import Lesson, Sound

class SoundTest(models.Model):
    """Represents a specific test
   
    Attributes:
        pairs_to_test (:class:`TestPair`): A list of pairs of which user sounds
        to send to which master sounds

        runs (:class:`TestConfig`): A list of test configurations associated w/ 
        each time this test was run
    """

    name = models.CharField(max_length=150)
    slug = models.SlugField(max_length=255)
    description = models.TextField()
    last_run = models.DateTimeField(editable=False, null=True, default=None)
    last_time = models.TimeField(
            verbose_name="time taken for last run",
            editable=False,
            null=True,
            default=None
    )
    date_created = models.DateTimeField(editable=False, auto_now=True)
    associated_lessons = models.ManyToManyField(Lesson)

    def save(self, *args, **kwargs):
        self.slug = slugify('{}_{}'.format(self.pk, self.name[:255]))
        super(SoundTest, self).save(*args, **kwargs)

    @property
    def master_sounds(self):
        pairs = self.pairs_to_test.select_related('master_sound')
        sounds = [sound for sound in Sound.objects.filter(pk__in=pairs.values_list('master_sound__pk').distinct())]
        return sounds

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'sound_tests'
        ordering = ['-last_run']

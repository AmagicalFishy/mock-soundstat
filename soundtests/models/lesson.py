from django.db import models

class Lesson(models.Model):
    """An object representing a TLO lesson

    Attributes:
        json (dict): A dictionary that mimics TLO's lesson JSON
    
    """

    lesson_name = models.CharField(max_length=150)
    json_name = models.CharField(max_length=150, default="", null=True)
    activity_type = models.CharField(max_length=20)
    learning_language = models.CharField(max_length=50)
    known_language = models.CharField(max_length=50)

    def __str__(self):
        return self.lesson_name

    @property
    def json(self):
        """Returns a dictionary that mimics TLO's Lesson JSON"""

        json_dict = {
                "listName": self.json_name,
                "languageLearn": self.learning_language,
                "languageNative": self.known_language,
                "files": [],
                "os": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
                "userName": "",
                "activityType": "pron"
        }
        for sound in self.associated_sounds.all():
            json_dict["files"].append(sound.json)
        
        return json_dict

    class Meta:
        db_table = 'lessons'


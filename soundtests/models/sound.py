from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from soundtests.models.lesson import Lesson

import urllib

class Sound(models.Model):
    """ Represents a particular sound uploaded from a user or a lesson

    This has all the information of sounds that are saved to the database and uploaded to
    Amazon. When searching for words to associate w/ a particular master-sound, or to include
    as a master sound, these will be what are searched, and should contain all relevant
    information for usage of the sound file. (Which, really, is just the S3 URL and lesson hash. 
    The other properties are for purposes of searching a database of these sounds).

    """

    SEX_CHOICES = (
            ('M', 'Male'),
            ('F', 'Female')
    )
    phrase1 = models.CharField(max_length=300)
    phrase2 = models.CharField(max_length=300)
    language = models.CharField(max_length=50)
    num_words = models.PositiveSmallIntegerField(verbose_name="number of words")
    num_syllables = models.PositiveSmallIntegerField(verbose_name="number of syllables")
    sex = models.CharField(max_length=1, choices=SEX_CHOICES)
    voice = models.BooleanField(verbose_name="synthetic voice")
    speaker_name = models.CharField(max_length=50)
    sound_hash = models.CharField(max_length=40, primary_key=True, editable=False)

    # Lesson Parameters
    lesson_url = models.URLField(max_length=300, null=True, default=None)
    lesson_hash = models.CharField(max_length=40, editable=False, null=True, default=None) # Lesson JSON doesn't use same hash
    associated_lesson = models.ForeignKey(
            Lesson, 
            on_delete=models.SET_NULL,
            verbose_name="associated lesson", 
            related_name="associated_sounds", 
            null=True, 
            default=None, 
            blank=True
    )
   
    @property
    def key(self):
        """Generate Amazon S3 key
        
        We use a property for key because it's made up of already populated
        fields, so there's no need to create an extra column in the DB

        """

        return ("sa_soundstat/sounds/{language}/{phrase2}/{sound_hash}.wav".format(
            language=self.language.replace(" ", "_"),
            phrase2=self.phrase2,
            sound_hash=self.sound_hash
        ))

    @property
    def url(self):
        """Generate public S3 URL or TLO URL if associated w/ a lesson"""
        if self.associated_lesson is not None: # Then sound is associated w/ a lesson
            return self.lesson_url
        else:
            return urllib.parse.quote('[REDACTED]'.format(self.key), safe=';/:@&=+')
    
    @property
    def json(self):
        """Returns a dictionary that mimics the 'files' section of the Lesson JSON 
        This method is used when constructing lesson JSON. If the sound instance
        is not associated with any lesson, return a dictionary whose only key is 
        "ERR" w/ an appropriate error message

        """

        if self.associated_lesson is not None:
            json_dict = {
                    "url": self.lesson_url,
                    "priority": 0,
                    "hash": self.lesson_hash,
                    "phrase1": self.phrase1,
                    "phrase2": self.phrase2,
                    "phraseTranslit": "",
                    "learningLanguageCode": self.associated_lesson.learning_language,
                    "knownLanguageCode": self.associated_lesson.known_language,
                    "precompareScore": 0
            }

        else:
            json_dict = {
                    "ERR": "This sound is not associated with a lesson"
            }

        return json_dict

    def __str__(self):
        return '{} : {}'.format(self.phrase1, self.phrase2)

    def save(self, bucket=None, *args, **kwargs):
        """Overriden save method to pass S3 bucket through. 
        
        When this method is called, we check to see if its 'phrase2' or its
        'language' parameter's have changed. If so, this method requires the
        additional 'bucket' argument.
        
        Args:
            bucket (boto3 bucket instance): An instance of boto3's bucket. This
            should be the same bucket on which the sound is hosted. Generally,
            this will be the bucket attribute of an instance of 
            :class:`utils.S3Connection`.

            """
        self.bucket = bucket
        super(Sound, self).save(*args, **kwargs)

    class Meta:
        db_table = 'sounds'

@receiver(pre_save, sender=Sound)
def change_s3_fields(sender, instance, **kwargs):
    """Changes name on S3 servers if fields that determine s3 Key change
   
    This does not take an argument itself, but an instance of Sound will
    not save unless an S3 bucket is passed to its save method if the fields
    "language" or "phrase2" have changed. (This is useful e.g. if sounds
    are uploaded to S3 with an incorrect phrase2 spelling)

    """

    try:
        sound = sender.objects.get(pk=instance.pk)
        bucket = instance.bucket
    except sender.DoesNotExist:
        pass # Sound is new

    else:
        if not sound.language == instance.language or not sound.phrase2 == instance.phrase2:
            if instance.bucket == None:
                raise TypeError(
                        "When changing a sound's language or phrase2 fields, "
                        "the save() method expects an instance of boto3's "
                        "S3.bucket representing the bucket this Sound is "
                        "currently saved to. Please pass this in as the keyword "
                        "argument 'bucket'."
                )
            else:
                source = {
                        'Bucket': bucket.name,
                        'Key': sound.key
                }
                bucket.copy(CopySource=source, Key=instance.key)
                bucket.delete_objects(Delete={'Objects': [{'Key': sound.key}]}) # What a god damned mess this syntax is
